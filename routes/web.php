<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//these pages can be viewed by anyone

Route::get('index', function () {
    return view('index');
});

Route::get('/', 'HomeController@index');

Route::get('agents', function (){
    return view('pages.agents');
});

Route::get('blog', function () {
    return view('pages.blog');
});

Route::get('blogdetail', function () {
    return view('pages.blogdetail');
});

//filter
Route::get('/properties/all_listing', 'properties_controller@all_listing');
Route::get('/properties/detail/{id}', 'properties_controller@property_detail');
Route::get('buy', 'properties_controller@buy');
Route::get('sale', 'properties_controller@sale');
Route::get('rent', 'properties_controller@rent');
Route::post('/properties/filter', 'properties_controller@filter');
Route::post('/properties/search', 'properties_controller@search');
Route::post('/enquiries/post_enquiry', 'contact_us@post_enquiry');
Route::post('/contact_us', 'contact_us@contact_us');
Route::post('/join', 'users_controller@join');
Route::post('/new_review', 'properties_controller@new_review');

Route::get('contact', function () {
    return view('pages.contact');
});

Route::get('test', function(){

    return view('visitors.test');
});

Route::get('detail', function () {
    return view('pages.detail');
});



Route::get('about', function () {
    return view('pages.about');
});

//these are member pages for authenticated users
//these pages are accessible only to authenticated users
Route::group(['middleware' => 'auth'], function () {

    Route::get('/home', 'HomeController@home');
    Route::get('/members/overview', 'HomeController@home');
    Route::get('/members/overview', 'HomeController@home');
    Route::get('/logout', 'Auth\LoginController@logout');

    Route::get('/members/my_profile', 'users_controller@my_profile');
    Route::get('/members/profile/{id}', 'users_controller@profile');
    Route::get('/members/my_profile/edit', 'users_controller@edit_form');
    Route::get('/members/profile/edit/{id}', 'users_controller@form');
    Route::post('members/profile/update', 'users_controller@update');
    Route::post('members/change_password', 'PasswordController@change_password');
    Route::post('/members/quick_mail', 'users_controller@quick_mail');
    Route::post('/members/users/create/new', 'users_controller@create');
    Route::post('/members/reports', ['uses'=>'reports_controller@show','as'=> '/members/reports']);
    Route::get('/members/enquiries/all', 'contact_us@enquiries_all');
    Route::get('/members/users/view', 'users_controller@view_users');
    Route::get('/members/properties/delete/{id}', 'properties_controller@delete');
    Route::get('/members/properties/edit/{id}', 'properties_controller@edit');
    Route::post('/members/properties/update', 'properties_controller@update');
    Route::get('/members/reports/auto', 'reports_controller@show_auto');

    Route::get('users', function () {
        return view('members.users');
    });

    Route::get('/members/properties', 'properties_controller@show_all');
    Route::post('/members/properties/create', 'properties_controller@create');


    Route::get('newIssues', function () {
        return view('members.newIssues');
    });

//Authenticated for agents and Admin
    Route::get('/members/reports', function () {
        return view('members.reports');
    });

    Route::get('mail', function () {
        return view('members.mail');
    });

    Route::get('support', function () {
        return view('members.support');
    });

    Route::get('newReports', function () {
        return view('members.newReports');
    });

    Route::get('reviews', function () {
        return view('members.reviews');
    });

    Route::get('notifications', function () {
        return view('members.notifications');
    });

    Route::get('registerUser', function () {
        return view('members.registerUser');
    });


//admin pages

    Route::get('system', function () {
        return view('members.admin.system');
    });

    Route::get('/members/admin/roles/view', 'admin_controller@roles_view');
    Route::post('/members/admin/roles/new', 'admin_controller@roles_new');
    Route::get('/members/admin/users/add/new', 'admin_controller@add_new_form');
    Route::get('/members/admin/users/delete/{id}', 'admin_controller@delete');
    Route::get('/members/admin/users/edit/{id}', 'users_controller@edit_form');
    Route::post('/members/admin/users/update', 'admin_controller@update');
    Route::get('/members/user/delete/{id}', 'users_controller@delete');

    Route::get('settings', function () {
        return view('members.admin.settings');
    });


//agents's pages


});

//authentications
Auth::routes();
