<!DOCTYPE>
<html>
<head>
    <title>New members</title>

    <style type="text/css">

        p{
            padding-left: 30px;
        }

        section {
            width: 50%;
            float: left;
            padding:10px;
            height: 270px;
            border-style: groove;
        }


        span{
            width: 38%;
            height: 270px;
            padding: 10px;
            float: right;
            border-style: groove;
        }

        article{
            width: 40%;
            height: 270px;
            padding:10px;

            border-style: groove;

        }

        table {
            width:100%;
        }
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        th, td {
            padding: 5px;
            text-align: center;
        }
        table tr:nth-child(even) {
            background-color: #eee;
        }
        table tr:nth-child(odd) {
            background-color:#fff;
        }
        table th {
            background-color: #222222;
            color: white;
        }

        footer {
            page-break-after: always;
            position: absolute;
            bottom:0;
            width:100%;
            height:60px;
            padding: 1rem;
            text-align: center;
        }

    </style>


    <h3 style="text-align: center"><img src="../public/images/logo.jpg"  width="120" height="132"><br><u><strong>{{$time_frame}} members record</strong></u></h3>

</head>
<body>


    @if($records !=[])
        @foreach($records as $record)
            <div>
    <p>User id: {{$record->user_id}}</p><br>
    <p>Names: {{$record->f_name}} {{$record->l_name}}</p><br>
    <p>Email: {{$record->email}}</p><br>
    <p>User type: {{$record->user_type_id}}</p><br>
    <p>Address: {{$record->location}}</p><br>
    <p>Contact:+ {{$record->contact}}</p><br>
    <p>Joined on: {{\Carbon\Carbon::parse($record->created_at)->toFormattedDateString()}}</p><br>

    <table>

        <thead>
        <tr>
            <th >Property id</th>
            <th>Name</th>
            <th >Type</th>
            <th>Rooms</th>
            <th>Location</th>
            <th>Price</th>
            <th>Status</th>
        </tr>
        </thead>

        @if($record->property !=[])
            @foreach($record->property as $property)
                <tr >
                    <td>{{$property->property_id}}</td>
                    <td>{{$property->property_name}}</td>
                    <td>{{$property->property_description}}</td>
                    <td>{{$property->number_of_rooms}}</td>
                    <td>{{$property->location}}</td>
                    <td>{{$property->price}}</td>
                    <td>{{$property->state}}</td>
                </tr>
    @endforeach
    @endif
    </table>

        <footer>
    <pre>
    <p><h6>Printed By:  <strong style="background:grey;">{{\Illuminate\Support\Facades\Auth::user()->f_name}}  {{\Illuminate\Support\Facades\Auth::user()->l_name}} </strong>      On: <strong style="background:grey;">{{\Carbon\Carbon::now()->toFormattedDateString()}}</strong></h6></p>
        </pre>
        </footer>

            </div>
        @endforeach
        @endif





</body>

</html>