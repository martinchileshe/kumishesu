<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
<p>Hi,</p>
<p>
    {{$request->message}}

</p>

<p>From: {{$request->full_name}}</p>
<p>Contact: {{$request->contact}}</p>
<p>Email: {{$request->email}}</p>

<a href="{{url('/properties/detail/'.$request->property_id)}}">View this property</a>

</body>
</html>
