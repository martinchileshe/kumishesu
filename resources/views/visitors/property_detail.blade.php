@extends('layouts.others')
@section('content')
    <!-- banner -->
    <div class="inside-banner">
        <div class="container">
            <span class="pull-right"><a href="{{url('/')}}">Home</a> / Buy</span>
            <h2>Buy</h2>
        </div>
    </div>
    <!-- banner -->


    <div class="container">
        <div class="properties-listing spacer">

            <div class="row">
                <div class="col-lg-3 col-sm-4 hidden-xs">

                    <div class="hot-properties hidden-xs">
                        <h4>Hot Properties</h4>
                        @if($hot_properties != [])
                            @foreach($hot_properties as $hot_property)
                                <div class="row">
                                    <div class="col-lg-4 col-sm-5"><img src="{{Storage::URL($hot_property->property->images->first()['filePath'])}}" class="img-responsive img-circle" alt="properties"></div>
                                    <div class="col-lg-8 col-sm-7">
                                        <h5><a href="{{url('/properties/detail/'.$hot_property->property_id)}}">{{$hot_property->property->property_description}}</a></h5>
                                        <p class="price">{{$hot_property->property->currentcy}}{{$hot_property->property->price}}</p> </div>
                                </div>
                            @endforeach
                        @endif

                    </div>



                    <div class="advertisement">
                        <h4>Advertisements</h4>
                        <img src="{{URL::asset('/images/advertisement.jpeg')}}" class="img-responsive" alt="advertisement">

                    </div>

                </div>

                <div class="col-lg-9 col-sm-8 ">

                    <h2>{{$property->number_of_rooms}} rooms {{$property->property_description}}</h2>
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="property-images">
                                <!-- Slider Starts -->
                                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                    <!-- Indicators -->
                                    <ol class="carousel-indicators hidden-xs">
                                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                        <li data-target="#myCarousel" data-slide-to="1" class=""></li>
                                        <li data-target="#myCarousel" data-slide-to="2" class=""></li>
                                        <li data-target="#myCarousel" data-slide-to="3" class=""></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <!-- Item 1 -->
                                        <div class="item active">
                                            <img src="{{Storage::URL($property->images->first()['filePath'])}}" class="properties" alt="properties" />
                                        </div>
                                        <!-- #Item 1 -->

                                        <!-- Item 2 -->
                                        @if($property->images != [])
                                            @foreach($property->images as $image)
                                                <div class="item">
                                                    <img src="{{Storage::URL($image->filePath)}}" class="properties" alt="properties" />

                                                </div>
                                                <!-- #Item 2 -->
                                            @endforeach
                                        @endif

                                        <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                                        <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                                    </div>
                                    <!-- #Slider Ends -->

                                </div>




                                <div class="spacer"><h4><span class="glyphicon glyphicon-th-list"></span> Property Details</h4>
                                    <p>{{$property->property_name}}</p>
                                    <p>@if($property->reviews->first() != []){{$property->reviews->first()->comment}}@endif</p>

                                </div>
                            <!--
                                <div><h4><span class="glyphicon glyphicon-map-marker"></span> Location</h4>
                                    <p>{$property->location}}</p>

                                    <div class="well"><iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Pulchowk,+Patan,+Central+Region,+Nepal&amp;aq=0&amp;oq=pulch&amp;sll=37.0625,-95.677068&amp;sspn=39.371738,86.572266&amp;ie=UTF8&amp;hq=&amp;hnear=Pulchowk,+Patan+Dhoka,+Patan,+Bagmati,+Central+Region,+Nepal&amp;ll=27.678236,85.316853&amp;spn=0.001347,0.002642&amp;t=m&amp;z=14&amp;output=embed"></iframe></div>
                                    -->
                                </div>

                            </div>
                            <div class="col-lg-4">
                                <div class="col-lg-12  col-sm-6">
                                    <div class="property-info">
                                        <div class="listing-detail">

                                            <div class="well well-sm">
                                                <div class="text-right">
                                                    <a id="open-review-box" href="#reviews-anchor" class="btn btn-link"><div id="hearts" class="starrr" data-rating='{{$property->reviews->first()['rating']}}'></div> </a>
                                                </div>

                                                <div class="row" id="post-review-box" style="display:none;">
                                                    <div class="col-md-12">
                                                        <form accept-charset="UTF-8" action="{{url('/new_review')}}" method="post">
                                                            {{ csrf_field() }}
                                                            <input id="ratings-hidden" name="rating" type="hidden">
                                                            <input id="property_hidden" name="property_id" value="{{$property->property_id}}" type="hidden">
                                                            <textarea class="form-control animated" cols="50" id="new-review" name="comment" placeholder="Enter your review here..." rows="5"></textarea>

                                                            <div class="text-right">
                                                                <div class="stars starrr" data-rating="0"></div>
                                                                <a class="btn btn-danger btn-sm" href="#" id="close-review-box" style="display:none; margin-right: 10px;">
                                                                    <span class="glyphicon glyphicon-remove"></span>Cancel</a>
                                                                <button class="btn btn-success btn-lg" type="submit">Save</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <p class="price">{{$property->currency}}{{$property->price}}</p>
                                        <p class="area"><span class="glyphicon glyphicon-map-marker"></span> {{$property->location}} Zambia</p>

                                        <div class="profile">
                                            <span class="glyphicon glyphicon-user"></span> Agent Details
                                            <p>{{$property->assigned_agent->f_name}} {{$property->assigned_agent->l_name}}<br>{{$property->assigned_agent->contact}}</p>
                                        </div>
                                    </div>

                                    <h6><span class="glyphicon glyphicon-adjust"></span> Availability</h6>


                                </div>
                                <div class="col-lg-12 col-sm-6 ">
                                    <div class="enquiry">
                                        <h6><span class="glyphicon glyphicon-envelope"></span>Ask a question</h6>
                                        <form role="form" method="POST" action="{{url('/enquiries/post_enquiry')}}">
                                            {{csrf_field()}}
                                            <input type="text" class="form-control" name="full_name" placeholder="Full Name" required/>
                                            <input type="email" class="form-control" name="email" placeholder="you@yourdomain.com" required/>
                                            <input type="text" class="form-control" name="contact" placeholder="your number" required/>
                                            <input type="hidden" class="form-control" name="property_id" value="{{$property->property_id}}" />
                                            <input type="hidden" class="form-control" name="agent_email" value="{{$property->assigned_agent->email}}"/>
                                            <textarea rows="6" class="form-control" name="message" placeholder="Message to agent" required></textarea>
                                            <button type="submit" class="btn btn-primary" name="Submit">Send Message</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection