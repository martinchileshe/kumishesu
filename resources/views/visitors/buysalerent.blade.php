@extends('layouts.others')

@section('content')
    <!-- banner -->
    <div class="inside-banner">
        <div class="container">
            <span class="pull-right"><a href="{{url('/')}}">Home</a> /{{$title}}</span>
            <h2>{{$title}}</h2>
        </div>
    </div>
    <!-- banner -->


    <div class="container">
        <div class="properties-listing spacer">

            <div class="row">
                <div class="col-lg-3 col-sm-4 ">

                    <div class="search-form"><h4><span class="glyphicon glyphicon-search"></span> Search for</h4>
                        <form role="form" method="POST" action="{{url('/properties/search')}}">
                            {{ csrf_field() }}
                            <input type="text" onkeydown="{{'/properties/search'}}" name="search_string" class="form-control" placeholder="Search of Properties">
                        </form>
                        <form role="form" method="POST" action="{{url('/properties/filter')}}">
                            {{ csrf_field() }}
                        <div class="row">
                            <div class="col-lg-5">
                                <select name="state" class="form-control">
                                    <option value="for sale">Buy</option>
                                    <option value="for rent">Rent</option>
                                    <option value="sold">Sale</option>
                                </select>
                            </div>
                            <div class="col-lg-7">
                                <select name="price" class="form-control">
                                    <option>Less than</option>
                                    <option value="1500">K1500</option>
                                    <option value="2500">K2500</option>
                                    <option value="30000">K30,000</option>
                                    <option value="100000">K100,000</option>
                                    <option value="500000">K500,000</option>
                                    <option value="1000000">K1000,000</option>
                                    <option value="10000000">K10,000,000</option>
                                    <option value="50000000">K50,000,000</option>
                                    <option value="100000000">K100,000,000</option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <select name="property_description" class="form-control">
                                    <option >Property Type</option>
                                    <option value="Apartment">Apartment</option>
                                    <option value="Building">Building</option>
                                    <option value="House">House</option>
                                    <option value="Office Space">Office Space</option>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Find Now</button>
                        </form>

                    </div>



                    <div class="hot-properties hidden-xs">
                        <h4>Hot Properties</h4>
                        @if($hot_properties != [])
                            @foreach($hot_properties as $hot_property)
                        <div class="row">
                            <div class="col-lg-4 col-sm-5"><img src="{{Storage::URL($hot_property->property->images->first()['filePath'])}}" class="img-responsive img-circle" alt="properties"></div>
                            <div class="col-lg-8 col-sm-7">
                                <h5><a href="{{url('/properties/detail/'.$hot_property->property_id)}}">{{$hot_property->property->property_description}}</a></h5>
                                <p class="price">{{$hot_property->property->currentcy}}{{$hot_property->property->price}}</p> </div>
                        </div>
                            @endforeach
                        @endif

                    </div>


                </div>

                <div class="col-lg-9 col-sm-8">
                    <div class="row">

                        <!-- properties -->
                        @if($properties != [])
                        @foreach($properties as $property)
                        <div class="col-lg-4 col-sm-6">
                            <div class="properties">
                                <div class="image-holder"><img src="{{Storage::URL($property->images->first()['filePath'])}}" class="img-responsive" alt="properties">
                                    <div class="status sold">{{$property->state}}</div>
                                </div>
                                <h4><a href="{{url('/properties/detail/'.$property->property_id)}}">{{$property->name}}</a></h4>
                                <p class="price">Price: {{$property->currency}}{{$property->price}}</p>
                                <div class="listing-detail"><div class="row lead">
                                        <div id="hearts" class="starrr" data-rating='{{$property->reviews->first()['rating']}}'></div>
                                        @if($property->reviews != [])
                                            <h5>{{$property->reviews->first()['comment']}}</h5>
                                        @endif
                                    </div></div>
                                <a class="btn btn-primary" href="{{url('/properties/detail/'.$property->property_id)}}">View Details</a>
                            </div>
                        </div>
                        <!-- properties -->
                        @endforeach
                        @endif
                        <div class="center">
                            <ul class="pagination">
                                {{$properties->links()}}
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection