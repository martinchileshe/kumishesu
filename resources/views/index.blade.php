@extends('layouts.others')
@section('content')
    <div class="">


        <div id="slider" class="sl-slider-wrapper">

            <div class="sl-slider">
@if($properties != [])
                @foreach($properties as $property)
                <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
                    <div class="sl-slide-inner">
                        <div style="background-image: url({{Storage::URL($property->images->first()['filePath'])}});" class="bg-img bg-img-1"></div>
                        <h2><a href="{{url('/properties/detail/'.$property->property_id)}}">{{$property->number_of_rooms}} Bed Room {{$property->property_description}} {{$property->state}}</a></h2>
                        <blockquote>
                            <p class="location"><span class="glyphicon glyphicon-map-marker"></span>{{$property->location}}</p>
                            <p>@if($property->reviews->first()!=[])
                            {{$property->reviews->first()->comment}}
                            @endif
                                <cite>{{$property->currency}}{{$property->price}}</cite>
                        </blockquote>
                    </div>
                </div>
                @endforeach
                @endif
            </div><!-- /sl-slider -->



            <nav id="nav-dots" class="nav-dots">
                <span class="nav-dot-current"></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </nav>

        </div><!-- /slider-wrapper -->
    </div>



    <div class="banner-search">
        <div class="container">
            <!-- banner -->
            <h3>Buy, Sale & Rent</h3>
            <div class="searchbar">
                <div class="row">
                    <div class="col-lg-6 col-sm-6">
                        <form role="form" method="POST" action="{{url('/properties/search')}}">
                            {{ csrf_field() }}
                            <input type="text" onkeydown="{{url('/properties/search')}}" name="search_string" class="form-control" placeholder="Search of Properties">
                            </form>
                        <form role="form" method="POST" action="{{url('/properties/filter')}}">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="col-lg-5">
                                    <select name="state" class="form-control">
                                        <option value="for sale">Buy</option>
                                        <option value="for rent">Rent</option>
                                        <option value="sold">Sale</option>
                                    </select>
                                </div>
                                <div class="col-lg-7">
                                    <select name="price" class="form-control">
                                        <option>Less than</option>
                                        <option value="1500">K1500</option>
                                        <option value="2500">K2500</option>
                                        <option value="30000">K30,000</option>
                                        <option value="100000">K100,000</option>
                                        <option value="500000">K500,000</option>
                                        <option value="1000000">K1000,000</option>
                                        <option value="1000000">K1000,000</option>
                                        <option value="1000000">K10,000,000</option>
                                        <option value="50000000">K50,000,000</option>
                                        <option value="100000000">K100,000,000</option>
                                    </select>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <select name="property_description" class="form-control">
                                        <option >Property Type</option>
                                        <option value="Apartment">Apartment</option>
                                        <option value="Building">Building</option>
                                        <option value="House">House</option>
                                        <option value="Office Space">Office Space</option>
                                    </select>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Find Now</button>
                        </form>


                    </div>
                    <div class="col-lg-5 col-lg-offset-1 col-sm-6 ">
                        <p>Join now and get updated with all the properties deals.</p>
                        <button id="" class="btn btn-info"   data-toggle="modal" data-target="#loginpop">Login</button>        </div>
                </div>
            </div>
        </div>
    </div>
    <!-- banner -->
    <div class="container">
        <div class="properties-listing spacer"> <a href="{{url('/properties/all_listing')}}" class="pull-right viewall">View All Listing</a>
            <h2>Featured Properties</h2>
            <div id="owl-example" class="owl-carousel">
                @foreach($properties as $property)
                    <div class="properties">
                        <div class="image-holder"><img
                                    src="{{Storage::URL($property->images->first()['filePath'])}}" style="height: 150px; width: 200px;" alt="properties"/>
                            <div class="status sold">{{$property->state}}</div>
                        </div>
                        <h4><a href="{{url('/property-detail/'.$property->property_id)}}">{{$property->name}}</a></h4>
                        <p class="price">{{$property->currency}}{{$property->price}}</p>
                        <div class="listing-detail">

                                <div class="row lead">
                                    <div id="hearts" class="starrr" data-rating='{{$property->reviews->first()['rating']}}'></div>
                                    @if($property->reviews != [])
                                    <h5>{{$property->reviews->first()['comment']}}</h5>
                                    @endif
                                </div>
                        </div>
                        <a class="btn btn-primary" href="{{url('/properties/detail/'.$property->property_id)}}">View Details</a>
                    </div>
                @endforeach
                    </div>
                </div>
            </div>
    </div>
    </div>
@endsection