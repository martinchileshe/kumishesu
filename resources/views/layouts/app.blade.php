<!DOCTYPE html>
<html lang="en">
<head>
    <title>Realestate Bootstrap Theme </title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <link rel="stylesheet" href="{{URL::asset('../assets/bootstrap/css/bootstrap.css')}}" />
    <link rel="stylesheet" href="{{URL::asset('assets')}}"/>
    <script src="{{URL::asset('http://code.jquery.com/jquery-1.9.1.min.js')}}"></script>
    <script src="{{URL::asset('../assets/bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{URL::asset('../assets/script.js')}}"></script>



    <!-- Owl stylesheet -->
    <link rel="stylesheet" href="{{URL::asset('../assets/owl-carousel/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{URL::asset('../assets/owl-carousel/owl.theme.css')}}">
    <script src="{{URL::asset('../assets/owl-carousel/owl.carousel.js')}}"></script>
    <!-- Owl stylesheet -->


    <!-- slitslider -->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('../assets/slitslider/css/style.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{URL::asset('../assets/slitslider/css/custom.css')}}" />
    <script type="text/javascript" src="{{URL::asset('../assets/slitslider/js/modernizr.custom.79639.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('../assets/slitslider/js/jquery.ba-cond.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('../assets/slitslider/js/jquery.slitslider.js')}}"></script>
    <!-- slitslider -->


</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top" style="background-color: #080808">
            <div class="container">
                <div class="navbar-header">
                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    </div>

                <div class="header">
                    <a href="{{url('/index')}}"><img style="margin-top: -50px; margin-bottom: -90px; margin-left: -110px;" src="images/logo.jpg" alt="Realestate"></a>
                </div>

                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>

        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="/js/app.js"></script>
</body>
</html>
