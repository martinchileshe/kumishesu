<!DOCTYPE html>
<html lang="en">
<head>
<title>Kumishesu real estates</title>
<meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <link rel="stylesheet" href="{{URL::asset('../assets/bootstrap/css/bootstrap.css')}}" />
    <link rel="stylesheet" href="{{URL::asset('../assets/style.css')}}"/>
    <script src="{{URL::asset('http://code.jquery.com/jquery-1.9.1.min.js')}}"></script>
    <script src="{{URL::asset('../assets/bootstrap/js/bootstrap.js')}}"></script>
    <script src="{{URL::asset('../assets/script.js')}}"></script>



    <!-- Owl stylesheet -->
    <link rel="stylesheet" href="{{URL::asset('../assets/owl-carousel/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{URL::asset('../assets/owl-carousel/owl.theme.css')}}">
    <script src="{{URL::asset('../assets/owl-carousel/owl.carousel.js')}}"></script>
    <!-- Owl stylesheet -->


    <!-- slitslider -->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('../assets/slitslider/css/style.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{URL::asset('../assets/slitslider/css/custom.css')}}" />
    <script type="text/javascript" src="{{URL::asset('../assets/slitslider/js/modernizr.custom.79639.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('../assets/slitslider/js/jquery.ba-cond.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('../assets/slitslider/js/jquery.slitslider.js')}}"></script>
    <!-- slitslider -->


</head>

<body>


<!-- Header Starts -->
<div class="navbar-wrapper">

        <div class="navbar-inverse" role="navigation">
          <div class="container">
            <div class="navbar-header">


              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>

            </div>


            <!-- Nav Starts -->
            <div class="navbar-collapse  collapse">
              <ul class="nav navbar-nav navbar-right">
               <li class="active"><a href="{{url('/')}}">Home</a></li>
                <li><a href="{{url('/about')}}">About</a></li>
                <li><a href="{{url('/agents')}}">Agents</a></li>
                <li><a href="{{url('/contact')}}">Contact</a></li>
              </ul>
            </div>
            <!-- #Nav Ends -->

          </div>
        </div>

    </div>
<!-- #Header Starts -->





<div class="container">

<!-- Header Starts -->
<div class="header">

    @if(Session::has('flash_message'))
        <div class="alert alert-success {{session()->has('flash_message_important')? session('flash_message') : ''}}">
            {{Session::get('flash_message')}}
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            @if(session()->has('flash_message_important'))

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            @endif
        </div>
    @endif

<a href="{{url('/')}}"><img style="margin-top: -20px; margin-bottom: -20px; margin-left: -10%;" src="{{URL::asset('/images/logo.jpg')}}" alt="Realestate"></a>

              <ul class="pull-right">
                <li><a href="{{url('/buy')}}">Buy</a></li>
                <li><a href="{{url('/contact')}}">Sell </a></li>
                <li><a href="{{url('/rent')}}">Rent</a></li>
              </ul>
</div>
<!-- #Header Starts -->
</div>

@yield('content')



<!--footer-->

<div class="footer">

    <div class="container">



        <div class="row">
            <div class="col-lg-3 col-sm-3">
                <h4>Information</h4>
                <ul class="row">
                    <li class="col-lg-12 col-sm-12 col-xs-3"><a href="{{url('/about')}}">About</a></li>
                    <li class="col-lg-12 col-sm-12 col-xs-3"><a href="{{url('/agents')}}">Agents</a></li>
                    <li class="col-lg-12 col-sm-12 col-xs-3"><a href="{{url('/contact')}}">Contact</a></li>
                </ul>
            </div>

            <!--
            <div class="col-lg-3 col-sm-3">
                <h4>Newsletter</h4>
                <p>Get notified about the latest properties in our marketplace.</p>
                <form class="form-inline" role="form">
                    <input type="text" placeholder="Enter Your email address" class="form-control">
                    <button class="btn btn-success" type="button">Notify Me!</button></form>
            </div>
            -->

            <div class="col-lg-3 col-sm-3">
                <h4>Follow us</h4>
                <a href="#"><img src="{{URL::asset('images/facebook.png')}}" alt="facebook"></a>
                <a href="#"><img src="{{URL::asset('images/twitter.png')}}" alt="twitter"></a>
                <a href="#"><img src="{{URL::asset('images/linkedin.png')}}" alt="linkedin"></a>
                <a href="#"><img src="{{URL::asset('images/instagram.png')}}" alt="instagram"></a>
            </div>

            <div class="col-lg-3 col-sm-3">
                <h4>Contact us</h4>
                <p><b>Kumishesu</b><br>
                    <span class="glyphicon glyphicon-map-marker"></span> <br>
                    <span class="glyphicon glyphicon-envelope"> info@kumishesu.com</span> <br>
                    <span class="glyphicon glyphicon-earphone"></span></p>
            </div>
        </div>
        <p class="copyright">Copyright {{\Carbon\Carbon::now()->year}}. All rights reserved.	</p>


    </div></div>



<!-- Modal -->
<div id="loginpop" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="row">
                <div class="col-sm-6 login">
                    <h4>Login</h4>
                    <form class="" role="form" action="{{url('/login')}}" METHOD="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="sr-only" for="exampleInputEmail2">Email address</label>
                            <input name="email" type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="exampleInputPassword2">Password</label>
                            <input name="password" type="password" class="form-control" id="exampleInputPassword2" placeholder="Password">
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}> Remember me
                            </label>
                        </div>
                        <button type="submit" class="btn btn-success">Sign in</button>
                        <a class="btn btn-link" href="{{ url('/password/reset') }}">
                            Forgot Your Password?
                        </a>
                    </form>
                </div>
                <div class="col-sm-6">
                    <h4>New User Sign Up</h4>
                    <p>Join today and get updated with all the properties deal happening around.</p>
                    <button class="btn btn-info"  onclick="window.location.href='{{url('/contact')}}'">Join Now</button>
                </div>

            </div>
        </div>

    </div>
</div>

</body>
</html>



