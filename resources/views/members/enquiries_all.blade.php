@extends('layouts.members')
@section('content')

    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">All users</div>

                <div class="panel-body">

                    <table class="table table-striped responsive-utilities" data-toggle="table"
                           data-show-refresh="false"
                           data-show-toggle="true" data-show-columns="true" data-search="true"
                           data-select-item-name="toolbar1" data-pagination="true"
                           data-sort-name="name"
                           data-sort-order="desc" style="font-size: small">

                        <thead>
                        <tr>
                            <!--<th data-field="state" data-checkbox="true">Count</th>-->
                            <th data-field="id" data-sortable="true">Property</th>
                            <th data-field="id" data-sortable="true">Type</th>
                            <th data-field="name" data-sortable="true">Contact</th>
                            <th data-field="rooms" data-sortable="true">Message</th>
                            <th data-field="position" data-sortable="true">Email</th>
                            <th data-field="authorized" data-sortable="true">On</th>
                            <th data-field="school" data-sortable="true">Reply/Delete</th>
                        </tr>
                        </thead>

                        @if($enquiries!=[])
                            @foreach($enquiries as $enquiry)

                                <tr>

                                    <td>{{$enquiry->property->property_id}}</td>
                                    <td>{{$enquiry->property->property_description}}</td>
                                    <td>{{$enquiry->contact}}</td>
                                    <td>{{$enquiry->message}}</td>
                                    <td>{{$enquiry->email}}</td>
                                    <td>{{\Carbon\Carbon::parse($enquiry->created_at)->toFormattedDateString()}}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-link btn-xs" onclick="set_email('{{$enquiry->email}}')"  data-toggle="modal" data-target="#reply"
                                               type="button" name="toggle" title="reply">
                                                <i class="glyphicon glyphicon glyphicon-send"></i>
                                            </a>
                                            <a class="btn btn-link btn-xs"
                                               onclick="delete_user('{{$enquiry->property->property_name}}', '{{$enquiry->property->property_id}}')"
                                               type="button" name="toggle" title="delete">
                                                <i class="glyphicon glyphicon glyphicon-trash"></i>
                                            </a>

                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </table>

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
        <!-- /.row -->
        <!-- /#page-wrapper -->

        <!--modal -->
        <form class="form-horizontal" role="form" method="POST"
              action="{{ url('/enquiries/reply/') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="modal fade" id="reply" role="dialog">
                <div class="modal-dialog modal-md">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button id="close" type="button" class="close"
                                    data-dismiss="modal">&times;</button>
                            <h4 class="modal-title text-primary">Reply</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">

                                    <div class="form-group">
                                        <label class="control-label" for="to">To</label>
                                        <input id="to" name="to" class="form-control"
                                               type="email" required autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="message">Message</label>
                                        <textarea id="message" name="message" placeholder="Type message here"
                                                  class="form-control" required autofocus></textarea>
                                    </div>
                                    <div class="modal-footer">
                                        <button class="btn btn-success" type="button" onclick="send_mail()">
                                            Send
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    <!-- scripts -->
        <script>
            function set_email(email) {
                $(document).ready(function () {
                    $('#to').val(email);

                });


            }
        </script>

        <script>
            function delete_user(user, id) {
                var xhttp;
                if (window.XMLHttpRequest) {
                    xhttp = new XMLHttpRequest();
                } else {
                    // code for IE6, IE5
                    xhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (confirm("Are you sure you want to delete " + user + "?")) {
                    xhttp.open("GET", "{{url('/admin/users/delete')}}/" + id, false);
                    xhttp.send();
                    //alert(user + " has been deleted!");
                    //location.reload();
                }

            }
        </script>

        <script>
            function send_mail(to, message) {
                $(document).ready(function () {


                    $("#button").click(function () {
                        $("#demo").toggle();
                    });
                    // process the form
                    $('#quickMail').submit(function (event) {
                        event.preventDefault();
                        alert('hi');


                        var formData = {
                            'to': $('input[name=to]').val(),
                            'message': $('input[name=message]').val(),
                            '_token': $('input[name=_token]').val()
                        };
                        // process the form
                        console.log('done');

                        $.ajax({
                            type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                            url: '{{ url('/enquiries/reply')}}', // the url where we want to POST
                            data: formData, // our data object
                            dataType: 'json', // what type of data do we expect back from the server
                            encode: true
                        })
                        // using the done promise callback
                                .done(function (data) {

                                    // log data to the console so we can see
                                    console.log(data);
                                    // here we will handle errors and validation messages
                                    // here we will handle errors and validation messages
                                    //if (data.success) {


                                    // ALL GOOD! just show the success message!
                                    //$('div.alert').append('<div class="alert alert-success">' + data.message + '</div>');
                                    $("#cloze").trigger('click');
                                    $("html, body").animate({scrollTop: 0}, 5);
                                    window.location.reload(true);
                                    // usually after form submission, you'll want to redirect
                                    // window.location = '/thank-you'; // redirect a user to another page
                                    // for now we'll just alert the user


                                });

                    });
                });


            }
        </script>

@endsection