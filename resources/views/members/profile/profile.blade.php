@extends('layouts.members')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{$user->f_name}}</div>

                <div class="panel-body">
                    <div class="col-md-9">
                        <form class="form-horizontal" role="form" style="margin-left: 20px;margin-top: 20px">
                            <div class="form-group">
                                <label class="col-sm-3 col-md-3 col-xs-4" for="first-name">First Name:</label>
                                <div class="col-sm-6 col-md-6 col-xs-5">
                                    <label class="text-primary" for="first_name_value">{{$user->f_name}}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 col-md-3 col-xs-4" for="last-name"> Last Name:</label>
                                <div class="col-sm-6 col-md-6 col-xs-5">
                                    <label class="text-primary" for="last_name_value">{{$user->l_name}}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 col-md-3 col-xs-4" for="e-mail"> Email Address:</label>
                                <div class="col-sm-6 col-md-6 col-xs-5">
                                    <label class="text-primary" for="e-mail_value">{{$user->email}}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 col-md-3 col-xs-4" for="man-number"> User id:</label>
                                <div class="col-sm-6 col-md-6 col-xs-5">
                                    <label class="text-primary" for="man-number_value">{{$user->user_id}}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 col-md-3 col-xs-4" for="NRC"> NRC Number:</label>
                                <div class="col-sm-6 col-md-6 col-xs-5">
                                    <label class="text-primary" for="NRC_value">{{$user->user_type_id}}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 col-md-3 col-xs-4" for="NRC"> NRC Number:</label>
                                <div class="col-sm-6 col-md-6 col-xs-5">
                                    <label class="text-primary" for="NRC_value">{{$user->NRC}}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 col-md-3 col-xs-4" for="residential-address"> Residential
                                    Address:</label>
                                <div class="col-sm-6 col-md-6 col-xs-5">
                                    <label class="text-primary"
                                           for="residential_address_value">{{$user->address}}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 col-md-3 col-xs-4" for="phone-number"> contact:</label>
                                <div class="col-sm-6 col-md-6 col-xs-5">
                                    <label class="text-primary" for="phone_number_value">{{$user->contact}}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 col-md-3 col-xs-4" for="contract-expiry-date">Registered
                                    on</label>
                                <div class="col-sm-6 col-md-6 col-xs-5">
                                    <label class="text-primary"
                                           for="contract_expiry_date_value">{{\Carbon\Carbon::parse($user->cretated_at)->toFormattedDateString()}}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-8 col-md-4 col-xs-6">
                                    <a href="{{url('/members/profile/edit/'.$user->user_id)}}"
                                       class="btn btn-social-icon btn-default btn-sm" role="button">Edit profile</a>
                                </div>

                                @if(Auth::user()->user_type_id!=="Property owner")
                                <div class="col-sm-8 col-md-4 col-xs-6">
                                    <a class="btn btn-info btn-sm" data-toggle="modal" data-target="#view_properties">View
                                        properties</a>
                                </div>
                                @endif
                                <div class="col-sm-8 col-md-4 col-xs-6">
                                    <a class="btn btn-yahoo btn-success btn-sm" data-toggle="modal"
                                       data-target="#contact">Send quick mail</a>
                                </div>
                            </div>

                        </form>
                        <!-- /.form -->
                    </div>

                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">

                        <div class="profile_img">

                            <!-- end of image cropping -->
                            <div id="crop-avatar">
                                <!-- Current avatar -->
                                <div class="avatar-view form-group" title="Add new profile picture">
                                    <a href="#" class="btn btn-link" role="button" data-toggle="modal"
                                       data-target="#avatar-modal" onclick="" id=""><img
                                                class="img-responsive img-circle"
                                                src="{{URL::asset('../img/profile.png')}}" alt="Avatar">
                                    </a>
                                </div>

                                <div id="demo" class="collapse">

                                </div>

                                <!-- Cropping modal -->
                                <div class="modal fade" id="avatar-modal" aria-hidden="true"
                                     aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <form class="avatar-form" action="crop.php" enctype="multipart/form-data"
                                                  method="post">
                                                <div class="modal-header">
                                                    <button class="close" data-dismiss="modal"
                                                            type="button">&times;</button>
                                                    <h4 class="modal-title" id="avatar-modal-label">Change Avatar</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="avatar-body">

                                                        <!-- Upload image and data -->
                                                        <div class="avatar-upload">
                                                            <input class="avatar-src" name="avatar_src" type="hidden">
                                                            <input class="avatar-data" name="avatar_data" type="hidden">
                                                            <label for="avatarInput">Local upload</label>
                                                            <input class="avatar-input" id="avatarInput"
                                                                   name="avatar_file" type="file">
                                                        </div>

                                                        <!-- Crop and preview -->
                                                        <div class="row">
                                                            <div class="col-md-9">
                                                                <div class="avatar-wrapper"></div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="avatar-preview preview-md"></div>
                                                            </div>
                                                        </div>

                                                        <div class="row avatar-btns">
                                                            <div class="col-md-9">
                                                                <div class="btn-group">
                                                                    <button class="btn btn-primary" data-method="rotate"
                                                                            data-option="-90" type="button"
                                                                            title="Rotate -90 degrees">Rotate Left
                                                                    </button>
                                                                    <button class="btn btn-primary" data-method="rotate"
                                                                            data-option="-15" type="button">-15deg
                                                                    </button>
                                                                    <button class="btn btn-primary" data-method="rotate"
                                                                            data-option="-30" type="button">-30deg
                                                                    </button>
                                                                    <button class="btn btn-primary" data-method="rotate"
                                                                            data-option="-45" type="button">-45deg
                                                                    </button>
                                                                </div>
                                                                <div class="btn-group">
                                                                    <button class="btn btn-primary" data-method="rotate"
                                                                            data-option="90" type="button"
                                                                            title="Rotate 90 degrees">Rotate Right
                                                                    </button>
                                                                    <button class="btn btn-primary" data-method="rotate"
                                                                            data-option="15" type="button">15deg
                                                                    </button>
                                                                    <button class="btn btn-primary" data-method="rotate"
                                                                            data-option="30" type="button">30deg
                                                                    </button>
                                                                    <button class="btn btn-primary" data-method="rotate"
                                                                            data-option="45" type="button">45deg
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <button class="btn btn-primary btn-block avatar-save"
                                                                        type="submit">Done
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- <div class="modal-footer">
                                                                  <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                                                                </div> -->
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.modal -->

                                <!-- Loading state -->
                                <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
                            </div>
                            <!-- end of image cropping -->

                        </div>

                    </div>

                </div>
                <!-- /.panel-body -->
                <!--  modal -->

                <form class="form-horizontal" role="form">
                    {{ csrf_field() }}
                    <div class="modal fade" id="view_properties" role="dialog">
                        <div class="modal-dialog modal-md">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button id="close" type="button" class="close"
                                            data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title text-primary">User properties</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">

                                            <table class="table table-striped responsive-utilities" data-toggle="table"
                                                   data-show-refresh="false"
                                                   data-show-toggle="true" data-show-columns="true" data-search="true"
                                                   data-select-item-name="toolbar1" data-pagination="true"
                                                   data-sort-name="name"
                                                   data-sort-order="desc" style="font-size: small">

                                                <thead>
                                                <tr>
                                                    <!--<th data-field="state" data-checkbox="true">Count</th>-->
                                                    <th data-field="id" data-sortable="true">Id</th>
                                                    <th data-field="name" data-sortable="true">Name</th>
                                                    <th data-field="rooms" data-sortable="true">Rooms</th>
                                                    <th data-field="position" data-sortable="true">Description</th>
                                                    <th data-field="authorized" data-sortable="true">Location</th>
                                                    <th data-field="school" data-sortable="true">Price</th>
                                                    <th data-field="renew by" data-sortable="true">Status</th>
                                                </tr>
                                                </thead>

                                                @if($user->property!=[])
                                                    @foreach($user->property as $property)

                                                        <tr>

                                                            <td>{{$property->property_id}}</td>
                                                            <td>{{$property->property_name}}</td>
                                                            <td>{{$property->number_of_rooms}}</td>
                                                            <td>{{$property->property_description}}</td>
                                                            <td>{{$property->location}}</td>
                                                            <td>{{$property->price}}</td>
                                                            <td>{{$property->state}}</td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- modal-->

                <!--  modal -->

                <form id="quickMail" class="form-horizontal" role="form">
                    {{ csrf_field() }}
                    <div class="modal fade" id="contact" role="dialog">
                        <div class="modal-dialog modal-md">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button id="cloze" type="button" class="close"
                                            data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title text-primary">Send mail</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                                            <div class="form-group">
                                                <label class="control-label" for="to">To</label>
                                                <input value="{{$user->email}}" id="to" name="to" class="form-control"
                                                       type="email" required autofocus>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label" for="message">Message</label>
                                                <textarea id="message" name="message" placeholder="Type message here"
                                                          class="form-control" required autofocus></textarea>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-success" type="button" onclick="send_mail()">
                                                    Send
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- modal-->

            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

    <script>
        function send_mail(to, message) {
            $(document).ready(function () {


                $("#button").click(function () {
                    $("#demo").toggle();
                });
                // process the form
                $('#quickMail').submit(function (event) {
                    event.preventDefault();
                    alert('hi');


                    var formData = {
                        'to': $('input[name=to]').val(),
                        'message': $('input[name=message]').val(),
                        '_token': $('input[name=_token]').val()
                    };
                    // process the form
                    console.log('done');

                    $.ajax({
                        type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
                        url: '{{ url('/members/quick_mail')}}', // the url where we want to POST
                        data: formData, // our data object
                        dataType: 'json', // what type of data do we expect back from the server
                        encode: true
                    })
                    // using the done promise callback
                            .done(function (data) {

                                // log data to the console so we can see
                                console.log(data);
                                // here we will handle errors and validation messages
                                // here we will handle errors and validation messages
                                //if (data.success) {


                                // ALL GOOD! just show the success message!
                                //$('div.alert').append('<div class="alert alert-success">' + data.message + '</div>');
                                $("#cloze").trigger('click');
                                $("html, body").animate({scrollTop: 0}, 5);
                                window.location.reload(true);
                                // usually after form submission, you'll want to redirect
                                // window.location = '/thank-you'; // redirect a user to another page
                                // for now we'll just alert the user


                            });

                });
            });


        }
    </script>



@endsection
