@extends('layouts.members')
@section('content')

    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">Properties</div>

                <div class="panel-body">

            <button id="new" class="btn btn-link" data-toggle="modal" data-target="#new_role">Define new role</button>

            <table class="table table-striped responsive-utilities" data-toggle="table" data-show-refresh="false"
                   data-show-toggle="true" data-show-columns="true" data-search="true"
                   data-select-item-name="toolbar1" data-pagination="true" data-sort-name="role_description"
                   data-sort-order="desc" style="font-size: small">

                <thead>
                <tr>
                    <th data-field="role_id" data-sortable="true">RoleId</th>
                    <th data-field="role_description" data-sortable="true">Description</th>
                    <th data-field="updated_at" data-sortable="true">Modified on</th>
                    <th data-field="edit/delete" data-sortable="true">edit/delete</th>
                </tr>
                </thead>

                    @if($roles!=[])
                        @foreach($roles as $role)
                        <tr>
                            <td>{{$role->role_id}}</td>
                            <td>{{$role->role_description}}</td>
                            <td>{{\Carbon\Carbon::parse($role->updated_at)->toFormattedDateString()}}</td>
                            <td>
                                <div class="btn-group">
                                    <a href="{{url('/members/admin/role/delete/'.$role->id)}}" class="btn btn-sm btn-link">Edit</a>
                                    <button class="btn btn-default btn-xs" onclick="delete_role('{{$role->role_description}}','{{$role->id}}')" type="button" name="toggle" title="delete">
                                        <i class="glyphicon glyphicon glyphicon-trash"></i>
                                    </button>

                                </div>
                            </td>
                        </tr>
                        @endforeach
                    @endif
            </table>

            <script>
                $(function () {
                    $('#hover, #striped, #condensed').click(function () {
                        var classes = 'table';

                        if ($('#hover').prop('checked')) {
                            classes += ' table-hover';
                        }
                        if ($('#condensed').prop('checked')) {
                            classes += ' table-condensed';
                        }
                        $('#table-style').bootstrapTable('destroy')
                                .bootstrapTable({
                                    classes: classes,
                                    striped: $('#striped').prop('checked')
                                });
                    });
                });

                function rowStyle(row, index) {
                    var classes = ['active', 'success', 'info', 'warning', 'danger'];

                    if (index % 2 === 0 && index / 2 < classes.length) {
                        return {
                            classes: classes[index / 2]
                        };
                    }
                    return {};
                }
            </script> <!--/. script-->

            <script>
                function delete_role(description, id) {
                    var xhttp;
                    if (window.XMLHttpRequest) {
                        xhttp = new XMLHttpRequest();
                    } else {
                        // code for IE6, IE5
                        xhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    if (confirm("Are you sure you want to delete " + description + "?")) {
                        xhttp.open("GET", "{{url('admin/roles/delete')}}/" + man, false);
                        xhttp.send();
                        alert(user + " has been deleted!");
                        location.reload();
                    }

                }
            </script>

        </div>
        <!-- /. ROW  -->
        <!-- /. PAGE INNER  -->
    </div>
    <!-- /. PAGE WRAPPER  -->

    <!-- Modal -->
    <form class="form-horizontal" role="form" action="{{url('/members/admin/roles/new')}}" method="POST">
        {{ csrf_field() }}
        <div class="modal fade" id="new_role" role="dialog">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button id="close" type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-primary">Define new role</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                                <div class="form-group">
                                    <label for="roleId">Role id</label>
                                    <input name="role_id" type="text" class="form-control" id="roleId" value="ROLE-{{random_int(100,999)}}">
                                </div>
                                <div class="form-group">
                                    <label for="role_description">Role description</label>
                                    <input name="role_description" type="text" class="form-control" id="role_description"
                                           placeholder="Add description of the role here">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-md- ">

                            <button type="submit" class="btn btn-default">Save</button>
                            <button type="reset" class="btn btn-default">Cancel</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </form>


    <!-- /.modal -->
    </div>
    <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
    <!-- /.row -->
    <!-- /#page-wrapper -->

    <!-- Custom Table JavaScript -->

@endsection