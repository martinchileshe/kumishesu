﻿@extends('layouts.members')
@section('content')

    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">Admin overview</div>

                <div class="panel-body">

                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                        <div class="div-square">
                            <a href="{{url('/system')}}">
                                <i class="fa fa-circle-o-notch fa-5x"></i>
                                <h4>System</h4>
                            </a>
                        </div>
                        </div>


                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                        <div class="div-square">
                            <a href="{{url('/mail')}}">
                                <i class="fa fa-envelope-o fa-5x"></i>
                                <h4>Mail Box</h4>
                            </a>
                        </div>


                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                        <div class="div-square">
                            <a href="{{url("/members/enquiries/all")}}">
                                <i class="fa fa-comments-o fa-5x"></i>
                                <h4>Enquiries</h4>
                            </a>
                        </div>


                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                        <div class="div-square">
                            <a href="{{url('/newUsers')}}">
                                <i class="fa fa-users fa-5x"></i>
                                <h4>New users</h4>
                            </a>
                        </div>


                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                        <div class="div-square">
                            <a href="{{url('/members/admin/roles/view')}}">
                                <i class="fa fa-key fa-5x"></i>
                                <h4>Roles</h4>
                            </a>
                        </div>


                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                        <div class="div-square">
                            <a href="{{url('/support')}}">
                                <i class="fa fa-comments-o fa-5x"></i>
                                <h4>Support</h4>
                            </a>
                        </div>


                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row text-center pad-top">

                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                        <div class="div-square">
                            <a class="btn btn-link" data-toggle="modal" data-target="#reports">
                                <i class="fa fa-clipboard fa-5x"></i>
                                <h4>Manual Reports</h4>
                            </a>
                        </div>


                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                        <div class="div-square">
                            <a href="{{url('/settings')}}">
                                <i class="fa fa-gear fa-5x"></i>
                                <h4>Settings</h4>
                            </a>
                        </div>


                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                        <div class="div-square">
                            <a href="{{url('/reviews')}}">
                                <i class="fa fa-wechat fa-5x"></i>
                                <h4>Reviews</h4>
                            </a>
                        </div>


                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                        <div class="div-square">
                            <a href="{{url('/notifications')}}">
                                <i class="fa fa-bell-o fa-5x"></i>
                                <h4>Notifications </h4>
                            </a>
                        </div>


                    </div>

                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                        <div class="div-square">
                            <a href="{{url('/members/admin/users/add/new')}}">
                                <i class="fa fa-user fa-5x"></i>
                                <h4>Register User</h4>
                            </a>
                        </div>


                    </div>
                </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->

            <!--modal -->
            <form class="form-horizontal" role="form" method="POST"
                  action="{{ url('/members/reports/') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal fade" id="reports" role="dialog">
                    <div class="modal-dialog modal-md">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button id="close" type="button" class="close"
                                        data-dismiss="modal">&times;</button>
                                <h4 class="modal-title text-primary">Reports</h4>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">

                                            <div class="row">
                                                <div class="col-lg-5">
                                                    <select id="duration" name="time_frame" class="form-control">
                                                        <option value="weekly">Weekly</option>
                                                        <option value="monthly">Monthly</option>
                                                        <option value="annual">Annual</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-7">
                                                    <select name="report_topic" class="form-control">
                                                        <option value="new_members">New members</option>
                                                        <option value="most_searched">Most searched for areas</option>
                                                        <option value="common_issues">Known issues</option>
                                                        <option value="rented_out">Just rented out</option>
                                                        <option value="sold">Just sold</option>
                                                    </select>
                                                </div>
                                            </div>

                                <div class="modal-footer">
                                    <div class="col-md- ">

                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="reset" class="btn btn-default">Cancel</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                        </div>
                    </div>
            </form>


    </div>
    <!-- /.col-lg-12 -->
    <!-- /.row -->
        </div>
    <!-- /#page-wrapper -->

    <!-- Custom Table JavaScript -->
@endsection