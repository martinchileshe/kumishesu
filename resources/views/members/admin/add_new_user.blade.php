@extends('layouts.members')

@section('content')

    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">Add new user</div>

                <div class="panel-body">

                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/members/users/create/new') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                                <label for="user_id" class="col-md-4 control-label">User id</label>

                                <div class="col-md-6">
                                    <input id="user_id" type="text" class="form-control" name="user_id" value="KUM-{{random_int(100,9999)}}" required autofocus>

                                    @if ($errors->has('user_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('user_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('NRC') ? ' has-error' : '' }}">
                                <label for="NRC" class="col-md-4 control-label">NRC</label>

                                <div class="col-md-6">
                                    <input id="NRC" type="text" class="form-control" name="NRC" value="{{old('NRC')}}" placeholder="nrc number" required autofocus>

                                    @if ($errors->has('NRC'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('NRC') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('user_type_id') ? ' has-error' : '' }}">
                                <label for="user_id" class="col-md-4 control-label">User type</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="user_type_id" required autofocus>
                                        <option value="">Select</option>
                                        @foreach($user_types as $user_type)
                                        <option value="{{$user_type->user_type_id}}">{{$user_type->user_type_id}}</option>
                                            @endforeach

                                    </select>

                                    @if ($errors->has('user_type_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('user_type_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('f_name') ? ' has-error' : '' }}">
                                <label for="f_name" class="col-md-4 control-label">First Name</label>

                                <div class="col-md-6">
                                    <input id="f_name" type="text" class="form-control" name="f_name" value="{{ old('f_name') }}" required autofocus>

                                    @if ($errors->has('f_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('f_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('l_name') ? ' has-error' : '' }}">
                                <label for="l_name" class="col-md-4 control-label">Last Name</label>

                                <div class="col-md-6">
                                    <input id="l_name" type="text" class="form-control" name="l_name" value="{{ old('l_name') }}" required autofocus>

                                    @if ($errors->has('l_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('l_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="contact" class="col-md-4 control-label">Contact</label>

                                <div class="col-md-6">
                                    <input id="contact" type="text" class="form-control" name="contact" placeholder="phone number" required autofocus>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="address" class="col-md-4 control-label">Residential address</label>

                                <div class="col-md-6">
                                    <input id="address" type="text" class="form-control" name="address" placeholder="residential address" required autofocus>
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Register
                                    </button>
                                </div>
                            </div>
                        </form>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
        <!-- /.row -->
        <!-- /#page-wrapper -->

        <!-- Custom Table JavaScript -->
@endsection
