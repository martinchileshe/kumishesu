@extends('layouts.members')
@section('content')
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">Edit property</div>

            <div class="panel-body">

<form class="form-horizontal" role="form" method="POST"
      action="{{ url('/members/properties/update') }}" enctype="multipart/form-data">
    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">


                                <div class="col-md-6">
                                    <input id="property_id" type="hidden" class="form-control"
                                           name="property_id" value="{{$property->property_id}}">
                                </div>

                            <div class="form-group{{ $errors->has('property_name') ? ' has-error' : '' }}">
                                <label for="property_name" class="control-label">Property description</label>

                                <div class="col-md-6">

                                                        <textarea class="form-control" name="property_name" id="property_name" onkeyup="countChar(this)" rows="3"
                                                                  cols="20" placeholder="Enter a short description here">{{$property->property_name}}</textarea>
                                    <div id="charNum"></div>

                                    <!-- script to count number of characters entered in textarea-->
                                    <script>
                                        function countChar(val) {
                                            var len = val.value.length;
                                            if (len >= 100) {
                                                val.value = val.value.substring(0, 100);
                                            } else {
                                                $('#charNum').text(100 - len);
                                            }
                                        }
                                        ;
                                    </script>

                                    <!-- script end-->

                                    @if ($errors->has('property_name'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('property_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="number_of_rooms" class="control-label">Number of rooms</label>

                                <div class="col-md-6">
                                    <input id="number_of_rooms" placeholder="Enter number of rooms" type="number" class="form-control"
                                           name="number_of_rooms"
                                           value="{{ $property->number_of_rooms}}" required autofocus>

                                </div>
                            </div>

                            <div class="form-group">
                                <label for="property_description"
                                       class="control-label">Propety type</label>

                                <div class="col-md-6">
                                    <select name="property_description" class="form-control" required autofocus>
                                        <option  value="{{ $property->property_description }}">{{ $property->property_description }}</option>
                                        <option value="Apartment">Apartment</option>
                                        <option value="Building">Building</option>
                                        <option value="House">House</option>
                                        <option value="Office Space">Office Space</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="agent"
                                       class="control-label">Property agent</label>

                                <div class="col-md-6">
                                    <select id="agent" name="agent" class="form-control">
                                        <option  value="{{ $property->agent}}">{{ $property->assigned_agent->f_name}} {{ $property->assigned_agent->l_name}}</option>
                                        @foreach($agents as $agent)
                                        <option value="{{$agent->user_id}}">{{$agent->f_name}}{{$agent->l_name}}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="location" class="control-label">Location</label>

                                <div class="col-md-6">
                                    <input id="location" placeholder="location of the property or address" type="text" class="form-control"
                                           name="location" value="{{$property->location}}" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="price" class="control-label">price per month</label>

                                <div class="col-md-6">

                                    <div class="row">

                                                            <span class="col-md-offset-1 col-md-4 input-group-addon">
                                                                <select id="price" class="input-group" name="currency">
                                                                    <option value="{{$property->currency}}">{{$property->currency}}</option>
                                                            <option value="K">K</option>
                                                            <option value="$">$</option>

                                                        </select></span>
                                        <div class="input-group col-md-offset-4 col-md-6">
                                            <input id="price" placeholder="Price per month" type="number" value="{{$property->price}}" class="input-group form-control"
                                                   name="price" required>
                                        </div>


                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="state" class="control-label">Status</label>

                                <div class="col-md-6">
                                    <select id="state" class="form-control" name="state">
                                        <option value="{{$property->state}}">{{$property->state}}</option>
                                        <option value="for sale">For sale</option>
                                        <option value="for rent">For rent</option>
                                        <option value="sold">Sold</option>
                                        <option value="on rent">Rented out</option>

                                    </select>

                                </div>
                            </div>

                            <div class="form-group">
                                <label for="state" class="control-label">Owner</label>

                                <div class="col-md-6">
                                    <select id="state" class="form-control" name="owner" required>
                                        <option value="{{$property->owner->user_id}}">{{$property->owner->f_name}} {{$property->owner->l_name}}</option>
                                        @if($users!=[])
                                            @foreach($users as $user)
                                                <option value="{{$user->user_id}}">{{$user->f_name }} {{$user->l_name}}</option>
                                            @endforeach
                                        @endif

                                    </select>

                                </div>
                            </div>

                            <!-- Upload image and data -->
                            <div class="hot-properties hidden-xs">
                                <h4>Images</h4>
                                @if($property->images!=[])
                                    @foreach($property->images as $image)
                                <div class="row">
                                    <div class="col-lg-4 col-md-12 col-sm-5">
                                        <img src="{{Storage::URL($image->filePath)}}" class="img-responsive img-thumbnail" alt="properties"/>
                                        <h5><label for="avatarInput">Local upload</label></h5>
                                        <p class="price"><input class="avatar-input" id="avatarInput" name="{{$image->id}}" type="file"></p>
                                </div>
                                    </div>
                                @endforeach
                                @endif

                            </div>

                        </div>
                        <input type="hidden" name="id" value="{{$property->id}}">
                    </div>

                    <div class="modal-footer">
                        <div class="col-md- ">

                            <button type="submit" class="btn btn-default">Save</button>
                            <button type="reset" class="btn btn-default">Cancel</button>
                        </div>
                        </div>

</form>
                        <!-- /.modal -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
            <!-- /.row -->
            <!-- /#page-wrapper -->

            <!-- Custom Table JavaScript -->
@endsection