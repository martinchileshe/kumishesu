@extends('layouts.members')
@section('content')
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">Reports</div>

                <div class="panel-body">

            <table class="table table-striped responsive-utilities" data-toggle="table" data-show-refresh="false"
                   data-show-toggle="true" data-show-columns="true" data-search="true"
                   data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name"
                   data-sort-order="desc" style="font-size: small">

                <thead>
                <tr>
                    <!--<th data-field="state" data-checkbox="true">Count</th>-->
                    <th data-field="budget" data-sortable="true">Budget</th>
                    <th data-field="name" data-sortable="true">Purpose</th>
                    <th data-field="id" data-sortable="true">Applicant</th>
                    <th data-field="position" data-sortable="true">Amount</th>
                    <th data-field="authorized" data-sortable="true">Authorised amount</th>
                    <th data-field="school" data-sortable="true">Authorisation</th>
                    <th data-field="renew by" data-sortable="true">Recommended by</th>
                    <th data-field="contract status" data-sortable="true">Status</th>
                    <th data-field="application stage" data-sortable="true">Created</th>
                    <th data-field="edit" data-sortable="true">Edit/Retire</th>
                </tr>
                </thead>

                <tr>

                    <!--<td data-field="state" data-checkbox="true">{$imprest->id}}</td>-->
                    <td>Mumba</td>
                    <td>House</td>
                    <td>Hp</td>
                    <td>Phone</td>
                    <td>Charger</td>
                    <td >Sim</td>
                    <td ></td>


                    <td ></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
        <!-- /.row -->
        <!-- /#page-wrapper -->

@endsection