@extends('layouts.members')
@section('content')

    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">Properties</div>

                <div class="panel-body">

                    <button class="btn btn-link" data-toggle="modal" data-target="#add_new">Add new</button>

                    <table class="table table-striped responsive-utilities" data-toggle="table"
                           data-show-refresh="false"
                           data-show-toggle="true" data-show-columns="true" data-search="true"
                           data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name"
                           data-sort-order="desc" style="font-size: small">

                        <thead>
                        <tr>
                            <!--<th data-field="state" data-checkbox="true">Count</th>-->
                            <th data-field="id" data-sortable="true">Id</th>
                            <th data-field="name" data-sortable="true">Name</th>
                            <th data-field="rooms" data-sortable="true">Rooms</th>
                            <th data-field="position" data-sortable="true">Description</th>
                            <th data-field="authorized" data-sortable="true">Location</th>
                            <th data-field="school" data-sortable="true">Price</th>
                            <th data-field="renew by" data-sortable="true">Status</th>
                            <th data-field="edit" data-sortable="true">Edit-Delete</th>
                        </tr>
                        </thead>

                        @if($properties!=[])
                            @foreach($properties as $property)

                                <tr>

                                    <td>{{$property->property_id}}</td>
                                    <td>{{$property->property_name}}</td>
                                    <td>{{$property->number_of_rooms}}</td>
                                    <td>{{$property->property_description}}</td>
                                    <td>{{$property->location}}</td>
                                    <td>{{$property->price}}</td>
                                    <td>{{$property->state}}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a class="btn btn-link btn-xs" href="{{url('/members/properties/edit/'.$property->property_id)}}"
                                               type="button" name="toggle" title="edit">
                                                <i class="glyphicon glyphicon glyphicon-edit"></i>
                                            </a>
                                            <a class="btn btn-link btn-xs"
                                                    onclick="delete_user('{{$property->property_name}}', '{{$property->property_id}}')"
                                                    type="button" name="toggle" title="delete">
                                                <i class="glyphicon glyphicon glyphicon-trash"></i>
                                            </a>

                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </table>


                    <form class="form-horizontal" role="form" method="POST"
                          action="{{ url('/members/properties/create') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="modal fade" id="add_new" role="dialog">
                            <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button id="close" type="button" class="close"
                                                data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title text-primary">Add property</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">

                                                <div class="form-group{{ $errors->has('property_id') ? ' has-error' : '' }}">
                                                    <label for="property_id" class="control-label">Property id</label>

                                                    <div class="col-md-6">
                                                        <input id="property_id" type="text" class="form-control"
                                                               name="property_id" value="PRO-{{random_int(100,999)}}"
                                                               required autofocus>

                                                        @if ($errors->has('property_id'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('property_id') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('property_name') ? ' has-error' : '' }}">
                                                    <label for="property_name" class="control-label">Property description</label>

                                                    <div class="col-md-6">

                                                        <textarea class="form-control" name="property_name" id="property_name" onkeyup="countChar(this)" rows="3"
                                                                  cols="20" placeholder="Enter a short description here">{{old('property_name')}}</textarea>
                                                        <div id="charNum"></div>

                                                        <!-- script to count number of characters entered in textarea-->
                                                        <script>
                                                            function countChar(val) {
                                                                var len = val.value.length;
                                                                if (len >= 100) {
                                                                    val.value = val.value.substring(0, 100);
                                                                } else {
                                                                    $('#charNum').text(100 - len);
                                                                }
                                                            }
                                                            ;
                                                        </script>

                                                        <!-- script end-->

                                                        @if ($errors->has('property_name'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('property_name') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('number_of_rooms') ? ' has-error' : '' }}">
                                                    <label for="number_of_rooms" class="control-label">Number of rooms</label>

                                                    <div class="col-md-6">
                                                        <input id="number_of_rooms" placeholder="Enter number of rooms" type="number" class="form-control"
                                                               name="number_of_rooms"
                                                               value="{{ old('number_of_rooms') }}" required autofocus>

                                                        @if ($errors->has('number_of_rooms'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('number_of_rooms') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('property_description') ? ' has-error' : '' }}">
                                                    <label for="property_description"
                                                           class="control-label">Property type</label>

                                                    <div class="col-md-6">
                                                        <select name="property_description" class="form-control" required autofocus>
                                                            <option  value="{{ old('property_description') }}">Select property type</option>
                                                            <option value="Apartment">Apartment</option>
                                                            <option value="Building">Building</option>
                                                            <option value="House">House</option>
                                                            <option value="Office Space">Office Space</option>
                                                        </select>

                                                        @if ($errors->has('property_description'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('property_description') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="agent"
                                                           class="control-label">Property agent</label>

                                                    <div class="col-md-6">
                                                        <select id="agent" name="agent" class="form-control" required autofocus>
                                                            <option  value="{{ old('agent') }}">Select an agent</option>
                                                            @foreach($agents as $agent)
                                                                <option value="{{$agent->user_id}}">{{$agent->f_name}} {{$agent->l_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                                                    <label for="location" class="control-label">Location</label>

                                                    <div class="col-md-6">
                                                        <input id="location" placeholder="location of the property or address" type="text" class="form-control"
                                                               name="location" value="{{ old('location') }}" required>

                                                        @if ($errors->has('location'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                                    <label for="price" class="control-label">Price</label>

                                                    <div class="col-md-6">

                                                        <div class="row">

                                                            <span class="col-md-offset-1 col-md-4 input-group-addon">
                                                                <select id="price" class="input-group" name="currency">
                                                            <option value="K">K</option>
                                                            <option value="$">$</option>

                                                        </select></span>
                                                            <div class="input-group col-md-offset-4 col-md-6">
                                                        <input id="price" placeholder="Price per month" type="number" class="input-group form-control"
                                                               name="price" required>
                                                            </div>


                                                        </div>

                                                        @if ($errors->has('price'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="state" class="control-label">Status</label>

                                                    <div class="col-md-6">
                                                        <select id="state" class="form-control" name="state">
                                                            <option value="for sale">For sale</option>
                                                            <option value="for rent">For rent</option>
                                                            <option value="sold">Sold</option>
                                                            <option value="on rent">Rented out</option>

                                                        </select>

                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="state" class="control-label">Owner</label>

                                                    <div class="col-md-6">
                                                        <select id="state" class="form-control" name="owner" required>
                                                            <option value="">Select the owner of this property</option>
                                                            @if($users!=[])
                                                            @foreach($users as $user)
                                                            <option value="{{$user->user_id}}">{{$user->f_name }} {{$user->l_name}}</option>
                                                            @endforeach
                                                                @endif

                                                        </select>

                                                    </div>
                                                </div>

                                                <!-- Upload image and data -->
                                                <div class="col-md-6 col-lg-6 avatar-upload{{ $errors->has('img1') ? ' has-error' : '' }}">
                                                    <input class="avatar-src" name="avatar_src" type="hidden">
                                                    <input class="avatar-data" name="avatar_data" type="hidden">
                                                    <label for="avatarInput">Local upload</label>
                                                    <input class="avatar-input" id="avatarInput" name="img1" type="file">
                                                    @if ($errors->has('price'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('img1') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                                <div class="col-md-6 col-lg-6 avatar-upload{{ $errors->has('img2') ? ' has-error' : '' }}">
                                                    <input class="avatar-src" name="avatar_src" type="hidden">
                                                    <input class="avatar-data" name="avatar_data" type="hidden">
                                                    <label for="avatarInput">Local upload</label>
                                                    <input class="avatar-input" id="avatarInput" name="img2" type="file">
                                                    @if ($errors->has('price'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('img2') }}</strong>
                                    </span>
                                                    @endif
                                                </div>

                                                <div class="col-md-6 col-lg-6 avatar-upload{{ $errors->has('img3') ? ' has-error' : '' }}">
                                                    <input class="avatar-src" name="avatar_src" type="hidden">
                                                    <input class="avatar-data" name="avatar_data" type="hidden">
                                                    <label for="avatarInput">Local upload</label>
                                                    <input class="avatar-input" id="avatarInput" name="img3" type="file">
                                                    @if ($errors->has('price'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('img3') }}</strong>
                                    </span>
                                                    @endif
                                                </div>

                                                <div class="col-md-6 col-lg-6 avatar-upload{{ $errors->has('img4') ? ' has-error' : '' }}">
                                                    <input class="avatar-src" name="avatar_src" type="hidden">
                                                    <input class="avatar-data" name="avatar_data" type="hidden">
                                                    <label for="avatarInput">Local upload</label>
                                                    <input class="avatar-input" id="avatarInput" name="img4" type="file">
                                                    @if ($errors->has('price'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('img4') }}</strong>
                                    </span>
                                                    @endif
                                                </div>

                                                <div class="col-md-6 col-lg-6 avatar-upload{{ $errors->has('img5') ? ' has-error' : '' }}">
                                                    <input class="avatar-src" name="avatar_src" type="hidden">
                                                    <input class="avatar-data" name="avatar_data" type="hidden">
                                                    <label for="avatarInput">Local upload</label>
                                                    <input class="avatar-input" id="avatarInput" name="img5" type="file">
                                                    @if ($errors->has('price'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('img5') }}</strong>
                                    </span>
                                                    @endif
                                                </div>

                                            </div>
                                        </div>

                                        <div class="modal-footer">
                                            <div class="col-md- ">

                                                <button type="submit" class="btn btn-default">Save</button>
                                                <button type="reset" class="btn btn-default">Cancel</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        </div>
        <!-- /.col-lg-12 -->
        <!-- /.row -->
        <!-- /#page-wrapper -->

    <script>
        function delete_user(user, property) {
            var xhttp;
            if (window.XMLHttpRequest) {
                xhttp = new XMLHttpRequest();
            } else {
                // code for IE6, IE5
                xhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            if (confirm("Are you sure you want to delete " + user + "?")) {
                xhttp.open("GET", "{{url('/members/properties/delete')}}/" + property, false);
                xhttp.send();
                alert(user + " has been deleted!");
                location.reload();
            }

        }
    </script>


@endsection