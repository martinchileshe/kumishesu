@extends('layouts.members')
@section('content')

    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">All users</div>

                <div class="panel-body">
                    <button class="btn btn-link" data-toggle="modal" data-target="#add_new">Add new</button>

                    <table class="table table-striped responsive-utilities" data-toggle="table"
                           data-show-refresh="false"
                           data-show-toggle="true" data-show-columns="true" data-search="true"
                           data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name"
                           data-sort-order="desc" style="font-size: small">

                        <thead>
                        <tr>
                            <!--<th data-field="state" data-checkbox="true">Count</th>-->
                            <th data-field="budget" data-sortable="true">User id</th>
                            <th data-field="name" data-sortable="true">First name</th>
                            <th data-field="id" data-sortable="true">Last name</th>
                            <th data-field="position" data-sortable="true">email</th>
                            <th data-field="authorized" data-sortable="true">Type</th>
                            @if(Auth::user()->user_type_id !=='Property owner')
                                <th data-field="edit" data-sortable="true">Edit-More-Delete</th>
                                @else
                                <th data-field="edit" data-sortable="true">More</th>
                                @endif
                        </tr>
                        </thead>

                        @if($users !=[])
                            @foreach($users as $user)
                                <tr href="#">
                                    <td>{{$user->user_id}}</td>
                                    <td>{{$user->f_name}}</td>
                                    <td>{{$user->l_name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->user_type_id}}</td>
                                    <td>
                                        <div class="btn-group">
                                        @if(Auth::user()->user_type_id !=='Property owner')

                                            <a class="btn btn-link btn-xs"
                                               href="{{url('/members/admin/users/edit/'.$user->user_id)}}" type="button"
                                               name="toggle" title="edit">
                                                <i class="glyphicon glyphicon glyphicon-edit"></i>
                                            </a>
                                            <a class="btn btn-default btn-xs btn-link"
                                               href="{{url('/members/profile/'.$user->user_id)}}" type="button"
                                               name="toggle" title="info">
                                                <i class="glyphicon glyphicon glyphicon-info-sign"></i>
                                            </a>
                                            <a class="btn btn-default btn-xs btn-link"
                                               onclick="delete_user('{{$user->f_name}}', '{{$user->user_id}}')"
                                               type="button" name="toggle" title="delete">
                                                <i class="glyphicon glyphicon glyphicon-trash"></i>
                                            </a>


                                            @else
                                            <a class="btn btn-default btn-xs btn-link"
                                               href="{{url('/members/profile/'.$user->user_id)}}" type="button"
                                               name="toggle" title="info">
                                                <i class="glyphicon glyphicon glyphicon-info-sign"></i>
                                            </a>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </table>

                    <script>
                        function delete_user(user, id) {
                            var xhttp;
                            if (window.XMLHttpRequest) {
                                xhttp = new XMLHttpRequest();
                            } else {
                                // code for IE6, IE5
                                xhttp = new ActiveXObject("Microsoft.XMLHTTP");
                            }
                            if (confirm("Are you sure you want to delete " + user + "?")) {
                                xhttp.open("GET", "{{url('/admin/users/delete')}}/" + id, false);
                                xhttp.send();
                                //alert(user + " has been deleted!");
                                //location.reload();
                            }

                        }
                    </script>

                    <!--  modal -->

                    <form class="form-horizontal" role="form" method="POST"
                          action="{{ url('/members/users/create/new') }}">
                        {{ csrf_field() }}
                        <div class="modal fade" id="add_new" role="dialog">
                            <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button id="close" type="button" class="close"
                                                data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title text-primary">Add user</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">

                                                <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                                                    <label for="user_id" class="col-md-4 control-label">User id</label>

                                                    <div class="col-md-6">
                                                        <input id="user_id" type="text" class="form-control"
                                                               name="user_id" value="KUM-{{random_int(100,9999)}}"
                                                               required autofocus>

                                                        @if ($errors->has('user_id'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('user_id') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('NRC') ? ' has-error' : '' }}">
                                                    <label for="NRC" class="col-md-4 control-label">NRC</label>

                                                    <div class="col-md-6">
                                                        <input id="NRC" type="text" class="form-control" name="NRC" value="{{old('NRC')}}" placeholder="nrc number" required autofocus>

                                                        @if ($errors->has('NRC'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('NRC') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('user_type_id') ? ' has-error' : '' }}">
                                                    <label for="user_id" class="col-md-4 control-label">User
                                                        type</label>

                                                    <div class="col-md-6">
                                                        <select class="form-control" name="user_type_id" required>
                                                            <option value="">Select</option>
                                                            @foreach($user_types as $user_type)
                                                                <option value="{{$user_type->user_type_id}}">{{$user_type->user_type_id}}</option>
                                                            @endforeach

                                                        </select>

                                                        @if ($errors->has('user_type_id'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('user_type_id') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('f_name') ? ' has-error' : '' }}">
                                                    <label for="f_name" class="col-md-4 control-label">First
                                                        Name</label>

                                                    <div class="col-md-6">
                                                        <input id="f_name" type="text" class="form-control"
                                                               name="f_name" value="{{ old('f_name') }}" required
                                                               autofocus>

                                                        @if ($errors->has('f_name'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('f_name') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('l_name') ? ' has-error' : '' }}">
                                                    <label for="l_name" class="col-md-4 control-label">Last Name</label>

                                                    <div class="col-md-6">
                                                        <input id="l_name" type="text" class="form-control"
                                                               name="l_name" value="{{ old('l_name') }}" required
                                                               autofocus>

                                                        @if ($errors->has('l_name'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('l_name') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                    <label for="email" class="col-md-4 control-label">E-Mail
                                                        Address</label>

                                                    <div class="col-md-6">
                                                        <input id="email" type="email" class="form-control" name="email"
                                                               value="{{ old('email') }}" required>

                                                        @if ($errors->has('email'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="contact" class="col-md-4 control-label">Contact</label>

                                                    <div class="col-md-6">
                                                        <input id="contact" type="text" class="form-control"
                                                               name="contact" placeholder="phone number" required
                                                               autofocus>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="address" class="col-md-4 control-label">Residential
                                                        address</label>

                                                    <div class="col-md-6">
                                                        <input id="address" type="text" class="form-control"
                                                               name="address" placeholder="residential address" required
                                                               autofocus>
                                                    </div>
                                                </div>

                                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                    <label for="password"
                                                           class="col-md-4 control-label">Password</label>

                                                    <div class="col-md-6">
                                                        <input id="password" type="password" class="form-control"
                                                               name="password" required>

                                                        @if ($errors->has('password'))
                                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="password-confirm" class="col-md-4 control-label">Confirm
                                                        Password</label>

                                                    <div class="col-md-6">
                                                        <input id="password-confirm" type="password"
                                                               class="form-control" name="password_confirmation"
                                                               required>
                                                    </div>
                                                </div>

                                            <!--add
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-4 control-label">Address</label>

                            <div class="col-md-6">
                                <textarea rows="6" class="form-control" placeholder="address" name="address" required></textarea>

                                @if ($errors->has('address'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                                                    </div>
                                                </div>

                                                -->

                                                <div class="modal-footer">
                                                    <div class="col-md- ">

                                                        <button type="submit" class="btn btn-default">Save</button>
                                                        <button type="reset" class="btn btn-default">Cancel</button>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- modal-->

                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
        <!-- /.row -->
        <!-- /#page-wrapper -->

        <!-- Custom Table JavaScript -->

        <script>
            function delete_user(user, property) {
                var xhttp;
                if (window.XMLHttpRequest) {
                    xhttp = new XMLHttpRequest();
                } else {
                    // code for IE6, IE5
                    xhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (confirm("Are you sure you want to delete " + user + "?")) {
                    xhttp.open("GET", "{{url('/members/user/delete')}}/" + property, false);
                    xhttp.send();
                    alert(user + " has been deleted!");
                    location.reload();
                }

            }
        </script>

@endsection