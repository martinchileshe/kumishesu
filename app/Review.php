<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    //
    //public $primaryKey = 'property_id';
    public $fillable = ['property_id', 'comment', 'rating'];

    public function property(){
        return $this->belongsTo('App\Property', 'property_id', 'property_id');
    }
}
