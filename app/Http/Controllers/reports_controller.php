<?php

namespace App\Http\Controllers;

use App\Review;
use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;
use Barryvdh\DomPDF\Facade as PDF;
class reports_controller extends Controller
{
    //show auto generated reports

    public function show_auto(){
        return view('members.agents.auto_reports');
}
    //determine what to show in the forms
    public function show(Request $request)
    {

        $time_frame = $request->time_frame;
        $report_topic = $request->report_topic;
        switch ($report_topic) {
            case 'new_members':
                return $this->getNewMembers($time_frame, $report_topic);
                break;
            case 'most_searched':
                //
                return $this->getMostSearched($request);
                break;
            case 'common_issues':
                //
            case 'rented_out':
                $rented_out = getRentedProperties($time_frame, $report_topic);
            case 'sold':
                //
            default :
        }

        //

    }

    //get newest members address less than a week ago
    public function getNewMembers($time_frame, $report_topic)
    {

        //
        switch ($time_frame) {

            case 'weekly':
                //
                return $this->show_report(User::where('created_at', '>', Carbon::now()->subWeek())->get(), 'Weekly', 'reports.new_members');
                break;
            case 'monthly':
                //
                return $this->show_report(User::where('created_at', '>', Carbon::now()->subMonth())->get(), 'Monthly', 'reports.new_members');
                break;
            case 'annual':
                //
                return $this->show_report(User::where('created_at', '>', Carbon::now()->subYear())->get(), 'Annual', 'reports.new_members');
                break;
            default:
                return null;

        }
    }

//get the rented properties
    public function getRentedProperties($time_frame, $report_topic)
    {
        return Property::where('state', '=', 'rented')->get();
    }

    public function show_report($records, $time_frame, $view)
    {

        $pdf = PDF::loadView($view, ['records' => $records,'time_frame'=>$time_frame]);
        return $pdf->stream('show_report.pdf');

    }

    //get newest members address less than a week ago
    public function getMostSearched($request)
    {

        //
        switch ($request->time_frame) {

            case 'weekly':
                //
                return $this->show_report(Review::where('created_at', '>', Carbon::now()->subWeek())
                ->where('rating', '>',3)->orderBy('rating', 'desc')->get(), 'Weekly', 'reports.most_searched');
                break;
            case 'monthly':
                //
                return $this->show_report(Review::where('created_at', '>', Carbon::now()->subMonth())
                    ->where('rating', '>',3)->orderBy('rating', 'desc')->get(), 'Monthly', 'reports.most_searched');
                break;
            case 'annual':
                //
                return $this->show_report(Review::where('created_at', '>', Carbon::now()->subYear())
                    ->where('rating', '>',3)->orderBy('rating', 'desc')->get(), 'Monthly', 'reports.most_searched');
                break;
            default:
                return null;

        }
    }


}
