<?php

namespace App\Http\Controllers;

use App\User;
use App\User_type;
use Illuminate\Http\Request;
use App\Role;
use Illuminate\Support\Facades\Redirect;

class admin_controller extends Controller
{
    //get all the roles from roles table
    //pass them to the roles view page
    public function roles_view()
    {
        $roles = Role::all();
        return view('members.admin.view_roles')->with('roles', $roles);
    }


    //creates new roles record
    //returns to roles view page
    public function roles_new(Request $request)
    {
        $this->validate_new_role($request);
        Role::create(['role_id' => $request->role_id,
            'role_description' => $request->role_description]);
        session()->flash('flash_message', 'saved');
        return Redirect::action('admin_controller@roles_view');
    }

    //validates new roles
    public function validate_new_role($request)
    {
        $this->validate($request, [
            'role_id' => 'required',
            'role_description' => 'required'
        ]);
    }

    //retrieve all users and view them
    public function view_users()
    {
        $users = User::all();
        return view('members.view_users')->with('users', $users);
    }

    //show form for adding new user
    public function add_new_form()
    {
        $user_types = User_type::all();
        return view('members.admin.add_new_user')->with('user_types', $user_types);
    }

    //edit user
    public function edit_user($user_id)
    {
        $user = User::firstOrNew(array('user_id' => $user_id));
        if (Auth::user()->position == "Contracts Officer") {
            return view('profile.edit_user')->with('user', $user);
        } else {
            return view('profile.edit')->with('user', $user);
        }


    }

    //display users profile
    public function my_profile()
    {

        $user = Auth::User();
        return view('profile.my_profile')->with('user', $user);

    }

    //edit user profile
    public function edit_profile()
    {
        $user = Auth::user();
        return view('profile.edit_profile')->with('user', $user);


    }

    //function to delete a user from the database
    public function delete($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        session()->flash('flash_message', 'Success');
        return Redirect::action('admin_controller@view_users');

    }

    //create new user record
    protected function create(Request $request)
    {
        User::create([
            'email' => $request->email,
            'user_id' => $request->user_id,
            'user_type_id' => $request->user_type_id,
            'f_name' => $request->f_name,
            'l_name' => $request->l_name,
            'password' => bcrypt($request->password),
        ]);
        return Redirect::action('users_controller@view_users');
    }
}
