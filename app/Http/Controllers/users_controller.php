<?php

namespace App\Http\Controllers;

use App\User_type;
use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\Mail;
use Redirect;

class users_controller extends Controller
{

    //retrieve users and view them
    public function view_users()
    {
        //all user if admin
        if(Auth::user()->user_type_id=="Admin"){
            $users = User::all();

            //property owner of the properties they manage if agent
        }elseif (Auth::user()->user_type_id=="Agent") {

            //view only the property owners
            $users = User::where('user_type_id', 'Property owner')->get();
        }else{
            $users = User::where('user_type_id', 'Agent')->get();
        }
        return view('members.view_users')->with(['users'=> $users, 'user_types'=>User_type::all()]);
    }

    //view current user profile
    public function my_profile()
    {

        $user = Auth::User();
        return view('members.profile.my_profile')->with('user', $user);

    }

    //view user profile
    public function profile($id)
    {

        $user = User::findOrFail($id);
        return view('members.profile.profile')->with('user', $user);

    }


    //load the edit form with data
    public function edit_form()
    {
        $user = Auth::user();
        return view('members.profile.edit_profile')->with('user', $user);


    }

    //load the edit form with data for user
    public function form($id)
    {
        $user = User::findOrFail($id);
        return view('members.profile.edit_profile')->with('user', $user);


    }

    //create new user record
    protected function create(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|unique:users',
            'NRC' => 'required|unique:users',
            'email' => 'required|email|max:255|unique:users',
        ]);
        User::create([
            'email' => $request->email,
            'user_id' => $request->user_id,
            'user_type_id' => $request->user_type_id,
            'f_name' => $request->f_name,
            'l_name' => $request->l_name,
            'password' => bcrypt($request->password),
            'NRC' => $request->NRC,
            'contact' => $request->contact,
            'address' => $request->address]);
        return Redirect::action('users_controller@view_users');
    }

    //stores profile changes to database
    public function update(Request $request)
    {
        $user = Auth::user();

        $this->validate($request, [
            'f_name' => 'required|max:255',
            'l_name' => 'required|max:255',
            'email' => 'sometimes|email|max:255|unique:users',
        ]);

        $user->fill(['f_name' => $request->f_name, 'l_name' => $request->l_name,
            'otherName' => $request->otherName, 'nationality' => $request->nationality,
            'address' => $request->address, 'user_type_id' => $request->user_type_id, 'NRC' => $request->nrcNumber,
        ]);

        if ($request->has('email')) {
            $user->email = $request->email;
        }


        $user->save();
        session()->flash('flash_message', 'Profile updated');
        Return Redirect::action('HomeController@index');

    }

    //delete record
    public function delete($id){
        $user = User::findOrFail($id);
        $user->delete();
        Return redirect()->back();
    }


    //send a quick email to property owner
    public function quick_mail(Request $request)
    {
        // validate the variables ======================================================
        // if any of these variables don't exist, add an error to the $errors array
        $data = array();

        $validator = Validator::make($request->all(), [
            'to' => 'required|email',

        ]);

        // return a response ===========================================================
        // if there are any errors in the errors array, return a success boolean of false
        if (!$validator->fails()) {
            Mail::send('Mails.quick_mail', [$request->message], function ($m) use ($request) {

                $m->to($request->to, 'Me')->subject('Mail from Kumishesu');
            });
            $data['success'] = true;

        } else {

            $data['success'] = false;
        }

        $data['errors'] = $validator->errors();

        // return all our request to an AJAX call
        return response()->json($data);

    }

    //show new memeber registration
}
