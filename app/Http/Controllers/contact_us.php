<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enquiry;
use Mail;

class contact_us extends Controller
{
    //
    //sends email to agent of property in question or save into enquiries table if no connection
    public function post_enquiry(Request $request){

        //check if there is a connection then send email
        if(self::is_connected()){

            Mail::send('Mails.enquiry', ['request'=>$request], function ($m) use ($request) {

                $m->to($request->agent_email, 'Me')->subject('Property enquiry');
            });

            //flash message on success
            session()->flash('flash_message', 'Email sent!');

        }else{
            session()->flash('flash_message', 'Not sent. Please check your internet connection and try again');
        }
        $this->save_enquiry($request);
        Return redirect()->back();

    }

    //save an enquiry
    public function save_enquiry($request){
        Enquiry::create(['property_id'=>$request->property_id, 'full_name'=>$request->full_name,
            'message'=>$request->message,'contact'=>$request->contact, 'email'=>$request->email]);
    }

    //check if there is an active internet connection
    public static function is_connected()
    {
        $connected = @fsockopen("www.gmail.com", 80);
        //website, port  (try 80 or 443)
        if ($connected) {
            $is_conn = true; //action when connected
            fclose($connected);
        } else {
            $is_conn = false; //action in connection failure
        }
        return $is_conn;

    }

    //sends email to agent of property in question or save into enquiries table if no connection
    public function contact_us(Request $request){

        //check if there is a connection then send email
        if(self::is_connected()){

            Mail::send('Mails.contact_us', ['request'=>$request], function ($m) use ($request) {

                $m->to($request->agent_email, 'Me')->subject('Enquiry');
            });

            //flash message on success
            session()->flash('flash_message', 'Email sent!');

        }else{
            session()->flash('flash_message', 'Not sent. Please check your internet connection and try again');
        }
        $this->save_enquiry($request);
        Return redirect()->back();

    }

    //save an enquiry



    //view all enquiries in a table
    public function enquiries_all(){
        $enquiries = Enquiry::all();
        return view('members.enquiries_all')->with('enquiries', $enquiries);
    }
}
