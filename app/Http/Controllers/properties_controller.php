<?php

namespace App\Http\Controllers;

use App\Enquiry;
use App\Review;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Property;
use App\Image;
use Illuminate\Support\Facades\Auth;
use Storage;
use Redirect;
use Mail;

class properties_controller extends Controller
{
    //
    public function show_all()
    {
        //choose what the user can see based on their user type id
        if (Auth::user()->user_type_id == 'Admin') {
            $properties = Property::all();

            //get users and and agents for assigning to new properties in the model
            $users = User::where('user_type_id', 'Property owner')->get();
            $agents = User::where('user_type_id', 'Agent')->get();
            return view('members/properties')
                ->with(['properties' => $properties,
                    'users' => $users, 'agents' => $agents]);

        } elseif (Auth::user()->user_type_id == 'Agent') {

            //get all properties that have been assigned to or where added by this agent
            $properties = Property::all();

            //get users and for assigning to new properties in the model
            $users = User::where('user_type_id', 'Property owner')->get();
            $agents = User::where('user_type_id', 'Agent')->get();
            return view('members/properties')
                ->with(['properties' => $properties,
                    'users' => $users, 'agents' => $agents]);
        }else{

            //get properties that belong to this agent
            $properties = Property::where('property_owner', Auth::user()->user_id)->get();

            //get users and and agents for assigning to new properties in the model
            $users = User::where('user_id', Auth::user()->user_id)->get();
            $agents = User::where('user_type_id', 'Agent')->get();
            return view('members/properties')
                ->with(['properties' => $properties,
                    'users' => $users, 'agents' => $agents]);

        }
}

    //show edit from
    public function edit($id)
    {
        $property = Property::findOrFail($id);

        if(Auth::user()->user_tyepe_id=="Admin") {
            $users = User::where('user_type_id', 'Property owner')->get();
            $agents = User::where('user_type_id', 'Agent')->get();
        }elseif(Auth::user()->user_type_id=='Agent'){
            //get users and for assigning to new properties in the model
            $users = User::where('user_type_id', 'Property owner')->get();
            $agents = User::where('user_id', Auth::user()->user_id)->get();
        }else{
            //get users and and agents for assigning to new properties in the model
            $users = User::where('user_id', Auth::user()->user_id)->get();
            $agents = User::where('user_type_id', 'Agent')->get();
        }

        return view('members/edit_property')
            ->with(['property' => $property, 'users' => $users, 'agents' => $agents]);
    }

    //create new entry for properties
    public function create(Request $request)
    {
//validate the images
        $this->validate($request, [
            'img1' => 'max:5000|mimes:jpeg,bmp,png',
            'img2' => 'max:5000|mimes:jpeg,bmp,png',
            'img3' => 'max:5000|mimes:jpeg,bmp,png',
            'img4' => 'max:5000|mimes:jpeg,bmp,png',
            'img5' => 'max:5000|mimes:jpeg,bmp,png',
        ]);
        Property::create(['property_id' => $request->property_id,
            'property_name' => $request->property_name,
            'number_of_rooms' => $request->number_of_rooms,
            'property_description' => $request->property_description,
            'location' => $request->location, 'currency' => $request->currency,
            'property_owner' => $request->owner, 'price' => $request->price,
            'state' => $request->state, 'agent' => $request->agent]);

        //method to save the images to disk
        $this->save_images_to_disk($request);

        //method to save the image names to database
        //$this->save_images_to_database($request);

        //flash message
        session()->flash('flash_message', 'Saved');
        return Redirect::action('properties_controller@show_all');
    }

    //update new entry for properties
    public function update(Request $request)
    {

        //validate the images
        $this->validate($request, [
            'img1' => 'max:5000|mimes:jpeg,bmp,png',
            'img2' => 'max:5000|mimes:jpeg,bmp,png',
            'img3' => 'max:5000|mimes:jpeg,bmp,png',
            'img4' => 'max:5000|mimes:jpeg,bmp,png',
            'img5' => 'max:5000|mimes:jpeg,bmp,png',
        ]);

        $property = Property::findOrFail($request->property_id);

        $property->fill(['property_id' => $request->property_id,
            'property_name' => $request->property_name,
            'number_of_rooms' => $request->number_of_rooms,
            'property_description' => $request->property_description,
            'location' => $request->location, 'currency' => $request->currency,
            'property_owner' => $request->owner, 'price' => $request->price,
            'state' => $request->state, 'agent' => $request->agent]);
        $property->save();

        //method to save the images to disk
        //$this->update_images($request, $property);

        //method to save the image names to database
        //$this->save_images_to_database($request);

        //flash message
        session()->flash('flash_message', 'Saved');
        return Redirect::action('properties_controller@show_all');
    }

    //saves the imgages
    public function save_images_to_disk(Request $request)
    {
        $path1 = $request->file('img1')->store('public/properties');
        $path2 = $request->file('img2')->store('public/properties');
        $path3 = $request->file('img3')->store('public/properties');
        $path4 = $request->file('img4')->store('public/properties');
        $path5 = $request->file('img5')->store('public/properties');

        Image::create(['property_id' => $request->property_id, 'name' => $request->img1, 'filePath' => $path1]);
        Image::create(['property_id' => $request->property_id, 'name' => $request->img2, 'filePath' => $path2]);
        Image::create(['property_id' => $request->property_id, 'name' => $request->img3, 'filePath' => $path3]);
        Image::create(['property_id' => $request->property_id, 'name' => $request->img4, 'filePath' => $path4]);
        Image::create(['property_id' => $request->property_id, 'name' => $request->img5, 'filePath' => $path5]);
    }

    //update the imgages
    public function update_images(Request $request, $property)
    {
        $images = $property->images;
        foreach ($images as $image){
            Storage::delete($image->filePath);

        }
        $path1 = $request->file('img1')->store('public/properties');
        $path2 = $request->file('img2')->store('public/properties');
        $path3 = $request->file('img3')->store('public/properties');
        $path4 = $request->file('img4')->store('public/properties');
        $path5 = $request->file('img5')->store('public/properties');

        Image::create(['property_id' => $request->property_id, 'name' => $request->img1, 'filePath' => $path1]);
        Image::create(['property_id' => $request->property_id, 'name' => $request->img2, 'filePath' => $path2]);
        Image::create(['property_id' => $request->property_id, 'name' => $request->img3, 'filePath' => $path3]);
        Image::create(['property_id' => $request->property_id, 'name' => $request->img4, 'filePath' => $path4]);
        Image::create(['property_id' => $request->property_id, 'name' => $request->img5, 'filePath' => $path5]);
    }

    //delete record
    public function delete($id){
        $user = Property::findOrFail($id);
        $user->delete();
        Return redirect()->back();
    }

    //retrieve and display properties based on the applied filters
    public function all_listing()
    {
        $properties = Property::paginate(12);
        $hot_properties = Review::where('rating', '>', 3)->orderBy('rating', 'desc')->take(6)->get();
        return view('visitors.buysalerent')->with(['properties' => $properties, 'hot_properties' => $hot_properties, 'title'=>'All']);
    }

    //retrieve and display properties based on the applied filters
    public function buy()
    {
        $properties = Property::where('state', 'for sale')->paginate(12);
        $hot_properties = Review::where('rating', '>', 3)->orderBy('rating', 'desc')->take(6)->get();
        return view('visitors.buysalerent')->with(['properties' => $properties, 'hot_properties' => $hot_properties, 'title'=>'Rent']);
    }

    //retrieve and display properties based on the applied filters
    public function rent()
    {
        $properties = Property::where('state', 'for rent')->paginate(12);
        $hot_properties = Review::where('rating', '>', 3)->orderBy('rating', 'desc')->take(6)->get();
        return view('visitors.buysalerent')->with(['properties' => $properties, 'hot_properties' => $hot_properties, 'title'=>'Rent']);
    }

    //retrieve and display properties based on the applied filters
    public function sale()
    {
        $properties = Property::where('state', 'sold')->paginate(12);
        $hot_properties = Review::where('rating', '>', 3)->orderBy('rating', 'desc')->take(6)->get();
        return view('visitors.buysalerent')->with(['properties' => $properties, 'hot_properties' => $hot_properties, 'title'=>'Sale']);
    }


    //filter
    public function filter(Request $request)
    {
        $results = null;
        if ($request->has('state') and $request->has('price') and $request->has('property_description')) {

            $results = Property::where('state', $request->state)
                ->where('property_description', $request->property_description)
                ->where('price', '<', $request->price)
                ->paginate(12);

        } elseif ($request->has('state') and $request->has('price')) {

            $results = Property::where('state', $request->state)
                ->where('price', '<', $request->price)
                ->paginate(12);

        } elseif ($request->has('state') and $request->has('property_description')) {

            $results = Property::whereIn('state', $request->state)
                ->where('property_description', $request->property_description)
                ->paginate(12);

        } elseif ($request->has('price') and $request->has('property_description')) {

            $results = Property::where('property_description', $request->property_description)
                ->where('price', '<', $request->price)
                ->paginate(12);

        } elseif ($request->has('state')) {

            $results = Property::where(
                'state', $request->state)
                ->paginate(12);

        } elseif ($request->has('price')) {

            $results = Property::where('price', '<', $request->price)
                ->paginate(12);

        } elseif ($request->has('property_description')) {

            $results = Property::where('property_description', $request->property->description)
                ->paginate(12);

        } else {
            $results = [];
        }

        $hot_properties = Review::where('rating', '>', 3)->orderBy('rating', 'desc')->take(6)->get();
        return view('visitors.buysalerent')->with(['properties' => $results, 'hot_properties' => $hot_properties, 'title'=>'Advanced search']);
    }

    //search_string with string
    public function search(Request $request)
    {

        $results = Property::where('property_description', 'LIKE', '%' . $request->search_string . '%')
            ->orWhere('property_name', 'LIKE', '%' . $request->search_string . '%')
            ->orWhere('number_of_rooms', 'LIKE', '%' . $request->search_string . '%')
            ->orWhere('location', 'LIKE', '%' . $request->search_string . '%')
            ->orWhere('price', 'LIKE', '%' . $request->search_string . '%')->paginate(12);

        $hot_properties = Review::where('rating', '>', 3)->orderBy('rating', 'desc')->take(6)->get();
        return view('visitors.buysalerent')->with(['properties' => $results, 'hot_properties' => $hot_properties,
            'title'=>'Results']);

    }

    //vie property details
    public function property_detail($id)
    {
        $property = Property::findOrFail($id);
        $hot_properties = Review::where('rating', '>', 3)
            ->orderBy('rating', 'desc')->take(6)
            ->get();
        return view('visitors.property_detail')
            ->with(['property' => $property, 'hot_properties' => $hot_properties]);
    }

    //save view review
    public function new_review(Request $request){
        Review::create(['rating'=>$request->rating, 'comment'=>$request->comment, 'property_id'=>$request->property_id]);
        Return redirect()->back();
    }
}
