<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Property;
use App\Enquiry;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $propeties = Property::all();
        return view('index')->with('properties', $propeties);
    }

    //choose the page to display based on user type
    public function home(){
        $user_type = Auth::user()->user_type_id;
        if ($user_type =='Admin') {
            return view('members.admin.overview');
        }elseif($user_type=='Agent') {
            return view('members.agents.overview');
        }elseif ($user_type=='Property owner') {
            return view('members.propertyOwners.overview');
        }else{
            $propeties = Property::all();
            $hot_properties = Review::where('rating' ,'>' ,3)->orderBy('rating', 'desc')->paginate(6);
            return view('index')->with(['properties'=> $propeties,'hot_properties'=>$hot_properties]);
    }
    }

}
