<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    //
    public $table = 'properties';
    public $primaryKey = 'property_id';
    public $incrementing = false;
    public $fillable = ['agent','property_owner','property_id', 'currency','property_name','number_of_rooms','property_description', 'location', 'price','state'];

    public function images(){

        return $this->hasMany('App\Image', 'property_id', 'property_id');
    }

    public function owner(){
        return $this->belongsTo('App\User',  'property_owner','user_id');
    }

    public function assigned_agent(){
        return $this->hasOne('App\User','user_id', 'agent');
    }

    public function reviews(){
        return $this->hasMany('App\Review', 'property_id', 'property_id');
    }

    public function enquiry(){
        return $this->hasMany('App\Enquiry','property_id');
    }
}
