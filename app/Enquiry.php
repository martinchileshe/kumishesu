<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
    //
    protected $fillable = ['property_id', 'email', 'full_name', 'message', 'contact'];

    public function property(){

        return $this->belongsTo('App\Property', 'property_id', 'property_id');
    }
}
