<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $incrementing = false;
    public $primaryKey = 'user_id';
    protected $fillable = [
        'email', 'password', 'f_name', 'l_name', 'user_id', 'user_type_id', 'contact', 'address', 'NRC'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function property(){
        return $this->hasMany('App\Property', 'property_owner');
    }
}
