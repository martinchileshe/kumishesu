The goal of KRE is to communicate

real-time estate needs to the masses on all platforms and devices. This will include payment

system integration.

KREWA should at its most bare minimum be able to;

● Enable users to list their real estate up for sale.

● Enable users to login and manage their profiles.

● Enable users to view and express interest in other properties.

● System will provide reports on the most view properties and demographics such areas of

interest.

Fat Client Architecture to enhance user experience and performance of KREWA