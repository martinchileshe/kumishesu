-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 31, 2017 at 08:07 PM
-- Server version: 5.7.17-0ubuntu0.16.04.1
-- PHP Version: 5.6.30-7+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Kumishesu`
--

-- --------------------------------------------------------

--
-- Table structure for table `enquiries`
--

CREATE TABLE `enquiries` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `property_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `enquiries`
--

INSERT INTO `enquiries` (`id`, `email`, `full_name`, `property_id`, `contact`, `message`, `created_at`, `updated_at`) VALUES
(3, 'chileshemartin@gmail.com', 'MArtin Chileshe', 'PRO-198', '+260977596381', 'Hey', '2017-03-18 23:10:51', '2017-03-18 23:10:51'),
(4, 'chileshemartin@gmail.com', 'MArtin Chileshe', 'PRO-198', '+260977596381', 'Hey', '2017-03-18 23:11:55', '2017-03-18 23:11:55'),
(5, 'chileshemartin@gmail.com', 'Martin Chileshe', 'PRO-198', '+259868674', 'Hi', '2017-03-20 17:17:35', '2017-03-20 17:17:35'),
(6, 'mc1a14@cs.unza.zm', 'Martin', 'PRO-198', '+260977596381', 'fghjk', '2017-03-22 16:26:06', '2017-03-22 16:26:06');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(10) UNSIGNED NOT NULL,
  `property_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `filePath` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `property_id`, `name`, `filePath`, `created_at`, `updated_at`) VALUES
(1, 'PRO-198', '/tmp/phpAAa8bB', 'public/properties/8a4193eb17eec82359bfdb409bf1eeb1.jpeg', '2017-03-15 23:13:58', '2017-03-15 23:13:58'),
(2, 'PRO-198', '/tmp/phpGV5M8w', 'public/properties/905cf8c34c67f8308e12f90c251c892e.jpeg', '2017-03-15 23:13:58', '2017-03-15 23:13:58'),
(3, 'PRO-198', '/tmp/phpMMYy5s', 'public/properties/28814dbf59005e2f4953ee62f76df0b6.jpeg', '2017-03-15 23:13:58', '2017-03-15 23:13:58'),
(4, 'PRO-198', '/tmp/php2G7y2o', 'public/properties/28814dbf59005e2f4953ee62f76df0b6.jpeg', '2017-03-15 23:13:58', '2017-03-15 23:13:58'),
(5, 'PRO-198', '/tmp/phpNmcJ3k', 'public/properties/69466e1f1d16c72b5130466a0c562245.jpeg', '2017-03-15 23:13:58', '2017-03-15 23:13:58'),
(6, 'PRO-941', '/tmp/phpcXXcZz', 'public/properties/2e0cf57210ba04fa063b9f76efdd661b.jpeg', '2017-03-16 09:47:30', '2017-03-16 09:47:30'),
(7, 'PRO-941', '/tmp/phpJB9zRI', 'public/properties/0e207c9ca599456977d780b207abfe5e.jpeg', '2017-03-16 09:47:31', '2017-03-16 09:47:31'),
(8, 'PRO-941', '/tmp/phpfcadKR', 'public/properties/6f3b61abc78afd5620a40958752548b9.jpeg', '2017-03-16 09:47:31', '2017-03-16 09:47:31'),
(9, 'PRO-941', '/tmp/phpWaCXC0', 'public/properties/d99e01277844ec363500df9db9bdf79f.jpeg', '2017-03-16 09:47:31', '2017-03-16 09:47:31'),
(10, 'PRO-941', '/tmp/php8L1xw9', 'public/properties/6d164462a2b41fb5cf8e6a5df2ccc841.jpeg', '2017-03-16 09:47:31', '2017-03-16 09:47:31'),
(11, 'PRO-369', '/tmp/phpbVTEAS', 'public/properties/28814dbf59005e2f4953ee62f76df0b6.jpeg', '2017-03-16 09:48:57', '2017-03-16 09:48:57'),
(12, 'PRO-369', '/tmp/phpWdvUEB', 'public/properties/0e207c9ca599456977d780b207abfe5e.jpeg', '2017-03-16 09:48:57', '2017-03-16 09:48:57'),
(13, 'PRO-369', '/tmp/phpr3YqJk', 'public/properties/2e0cf57210ba04fa063b9f76efdd661b.jpeg', '2017-03-16 09:48:57', '2017-03-16 09:48:57'),
(14, 'PRO-369', '/tmp/phpzKb5N3', 'public/properties/8a4193eb17eec82359bfdb409bf1eeb1.jpeg', '2017-03-16 09:48:57', '2017-03-16 09:48:57'),
(15, 'PRO-369', '/tmp/phpW4C2SM', 'public/properties/6d164462a2b41fb5cf8e6a5df2ccc841.jpeg', '2017-03-16 09:48:57', '2017-03-16 09:48:57');

-- --------------------------------------------------------

--
-- Table structure for table `issues`
--

CREATE TABLE `issues` (
  `issues_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `issues_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2017_02_14_231321_users_table', 1),
(2, '2017_02_14_234313_password_reset_table', 1),
(3, '2017_02_15_012301_properties_table', 1),
(4, '2017_02_15_012407_user_types_table', 1),
(5, '2017_02_15_012430_roles_table', 1),
(6, '2017_02_15_012458_rights_table', 1),
(7, '2017_02_15_012524_reviews_table', 1),
(8, '2017_02_15_012553_issues_table', 1),
(9, '2017_02_17_110118_properties_for_rent_table', 1),
(10, '2017_02_17_110135_properties_for_sale_table', 1),
(11, '2017_02_17_110143_properties_sold_table', 1),
(12, '2017_02_17_110153_properties_on_rent_table', 1),
(13, '2017_02_25_160214_create_images_table', 1),
(14, '2017_12_01_222424_property_images_table', 1),
(15, '2017_03_16_110034_reviews_table', 2),
(16, '2017_03_16_111114_reviews_table', 3),
(17, '2017_03_19_003354_enquiries', 4),
(18, '2017_03_19_010020_enquiries', 5);

-- --------------------------------------------------------

--
-- Table structure for table `password_reset`
--

CREATE TABLE `password_reset` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `property_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `property_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `number_of_rooms` int(11) NOT NULL,
  `property_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `currency` enum('K','$') COLLATE utf8_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `state` enum('for sale','for rent','sold','on rent') COLLATE utf8_unicode_ci NOT NULL,
  `property_owner` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agent` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`property_id`, `property_name`, `number_of_rooms`, `property_description`, `location`, `currency`, `price`, `state`, `property_owner`, `agent`, `created_at`, `updated_at`) VALUES
('PRO-198', 'House', 7, 'Building', 'Chilenje', 'K', 7000, 'for rent', 'KUM-4874', 'KUM-2122', '2017-03-15 23:13:57', '2017-03-15 23:13:57'),
('PRO-369', 'Building', 9, 'Building', 'Chelstone', 'K', 10000, 'sold', 'KUM-4874', 'KUM-1687', '2017-03-16 09:48:57', '2017-03-16 09:48:57'),
('PRO-941', 'Office', 8, 'Office Space', 'Town', '$', 5000, 'for sale', 'KUM-4265', 'KUM-2122', '2017-03-16 09:47:30', '2017-03-16 09:47:30');

-- --------------------------------------------------------

--
-- Table structure for table `property_images`
--

CREATE TABLE `property_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `property_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `property_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `comment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rating` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `property_id`, `comment`, `rating`, `created_at`, `updated_at`) VALUES
(1, 'PRO-198', 'Okay', 4.5, '2017-03-15 22:00:00', '2017-03-15 22:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `rights`
--

CREATE TABLE `rights` (
  `user_type_id` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `role_description` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `f_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `l_name` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `contact` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `NRC` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `f_name`, `l_name`, `email`, `password`, `remember_token`, `user_type_id`, `contact`, `NRC`, `address`, `created_at`, `updated_at`) VALUES
('KUM-5110', 'Martin', 'Chileshe', 'chileshemartin@gmail.com', '$2y$10$7qYrXU8wPfJqLs1N/4zhXeQanmdyZ7dyuRApmXbL8ox4iuamKDeaG', 'ieGfcJbFSs0ns71DHVmuoi6h53RKzGLGrPer2hCrS8PrzkPGCT2OwMYyfXb0', 'Admin', '+260976132729', '123851/94/1', 'Avondale 1435', '2017-03-15 22:39:14', '2017-03-22 16:44:59'),
('KUM-4265', 'Hammond', 'Hamond', 'hamond@gmail.com', '$2y$10$ZINdjD.VzEKsl1Qs2Ag4cuUVIuGwaNS23iVJtwt.rMFSOQuLRPJh6', 'fnzVu7z8JDRnDuDXWDNVZQzCBchCFQJwZlJjXiWY2T3SOjmaGGXeDiu45EFP', 'Property owner', '+2609764787', '09894/94/1', 'Meanwood', '2017-03-16 09:41:50', '2017-03-22 17:05:08'),
('KUM-1687', 'Mwape', 'Malama', 'malama@gmail.com', '$2y$10$WEc1pmC3H/eaHvsVryQwj.da55a2Fj0oaKUH39hV8DWZeVOZfKG1m', 'Tdp6zIrBG8Ur4edkAciEO5N3h96sbcDpzA1EzkJB6yABJQSac4XmOU2R22HJ', 'Agent', '+260978373', '123874/94/1', 'Obama', '2017-03-16 09:40:38', '2017-03-22 18:50:13'),
('KUM-2122', 'Mumba', 'Chileshe', 'mc1a14@cs.unza.zm', '$2y$10$XTEKlYBdB67kJ/ioFFZEiunkrNIHTQfSO/N2nnWH8irTC9KMAVJju', NULL, 'Agent', '+260976132729', '187383/93/0', 'Avondale 1435', '2017-03-15 22:45:43', '2017-03-15 22:45:43'),
('KUM-2302', 'Charles', 'Mubanga', 'mubanga@gmail.com', '$2y$10$Kr/GNPo17FTVSuNn3EoerOLbOTvXt8cUGbwGJ8rdAKN3FUknocDbG', NULL, 'Agent', '+260903949', '637823/62/1', 'Kabwe', '2017-03-15 23:45:46', '2017-03-15 23:45:46'),
('KUM-4874', 'Kangwa', 'Kafula', 'mwisechileshe@gmail.com', '$2y$10$cAdWbZInVE09Znl/V82VsuAZ0g.tE1c.lT6DYjRf1.zpHP4GWZgcW', '11eFxdQS9WqPcc6XMBQNE8qutkx2TANfjWGqJ7nWlwVVsbcov5ULu2YY4kPY', 'Property owner', '+260976132729', '1729838/94/1', 'Kabwata', '2017-03-15 22:49:57', '2017-03-22 18:34:35');

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `user_type_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_description` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`user_type_id`, `user_description`, `created_at`, `updated_at`) VALUES
('Admin', 'administartor', '2017-03-15 22:00:00', '2017-03-15 22:00:00'),
('Property Owner', 'Owner of a property', '2017-03-15 22:00:00', '2017-03-15 22:00:00'),
('Agent', 'an agent', '2017-03-15 22:00:00', '2017-03-15 22:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `enquiries`
--
ALTER TABLE `enquiries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reset`
--
ALTER TABLE `password_reset`
  ADD KEY `password_reset_email_index` (`email`),
  ADD KEY `password_reset_token_index` (`token`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD UNIQUE KEY `properties_property_id_unique` (`property_id`);

--
-- Indexes for table `property_images`
--
ALTER TABLE `property_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reviews_property_id_foreign` (`property_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD UNIQUE KEY `roles_role_id_unique` (`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `enquiries`
--
ALTER TABLE `enquiries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `property_images`
--
ALTER TABLE `property_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_property_id_foreign` FOREIGN KEY (`property_id`) REFERENCES `properties` (`property_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
