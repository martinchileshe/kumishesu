<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('user_id', 10);
            $table->string('f_name', 60);
            $table->string('l_name', 60);
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->string('user_type_id', 20);
            $table->string('contact',20);
            $table->string('NRC',15);
            $table->string('address',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
