<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->string('property_id', 10)->unique();
            $table->string('property_name', 255);
            $table->integer('number_of_rooms');
            $table->string('property_description', 255);
            $table->string('location', 255);
            $table->enum('currency', ['K','$']);
            $table->double('price',10);
            $table->enum('state', ['for sale', 'for rent','sold','on rent']);
            $table->string('property_owner',10)->nullable();
            $table->string('agent',10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
